# TasksClient


Delphi implementation of [Google Tasks API](https://developers.google.com/tasks/).

## Prerequisite
This library was written in Delphi 10.2 Tokyo, but should be compatible from XE8. Also it uses [Spring4D](https://bitbucket.org/sglienke/spring4d/src/master/) (its base package), so please install it beforehand. For serialization it uses [JsonDataObjects](https://github.com/ahausladen/JsonDataObjects) unit, which is included explicitly to this project.

## Installation
Add following folders to your Delphi project's search path/IDE Library browsing path:

 - ..\Source
 - ..\Source\Common
 - ..\Source\Types
 - ..\Source\ThirdParty\JsonDataObjects
 - ..\Source\Client

## Usage
 Before start using this library, you must [register](https://developers.google.com/tasks/firstapp#register)  your project in Google APIs Console. After that you can acquire `credentials.json` file with your client id and client secret.
```
 var 
   client: ITasksClient;
   tasksService: ITaskListsService;
   tasks: IGoogleTaskCollection;
begin
  client := TasksClient.Create(TClientSecrets.ReadFromCredentials('path_to_credentials.json'));
  tasksService := client.TasksService;
  tasks := tasksClient.List('@default');
  
```
## TODO
 - Unit tests
 - Async requests
 - Remove RESTComponents dependencies and use `System.Net.HttpClient` directly (XE8 and up)
 - Add `Indy` implementation (compatibility with earlier versions of Delphi)
 
