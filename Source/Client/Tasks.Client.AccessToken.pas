unit Tasks.Client.AccessToken;

interface

uses
  System.Classes;

type
  TToken = class(TPersistent)
  public const
    cExpiryTolerance = 1;
    cPropAccessToken = 'access_token';
    cPropRefreshToken = 'refresh_token';
    cPropExpiresIn = 'expires_in';

    cDefaultFileDir = 'TasksClient';
    cDefaultFileName = 'tasks_access_token.json';
  private
    fAccessToken: string;
    fRefreshToken: string;
    fExpiryDate: TDateTime;
    procedure SetExpiresIn(const Value: Cardinal);
    function GetExpiryDateUtc: TDateTime;
    procedure SetExpiryDateUtc(const Value: TDateTime);
  public
    procedure Assign(Source: TPersistent); override;

    procedure LoadFromFile(fileName: string = '');
    procedure SaveToFile(fileName: string = '');

    function IsExpired: Boolean;
    function IsValid: Boolean;

    property ExpiresIn: Cardinal write SetExpiresIn;
    property ExpiryDate: TDateTime read fExpiryDate write fExpiryDate;
  published
    property AccessToken: string read fAccessToken write fAccessToken;
    property RefreshToken: string read fRefreshToken write fRefreshToken;
    property ExpiryDateUtc: TDateTime read GetExpiryDateUtc write SetExpiryDateUtc;
  end;

implementation

uses
  System.DateUtils,
  System.SysUtils,
  System.IOUtils,
  JsonDataObjects;

type
  TTimeZoneHelper = class helper for TTimeZone
  public
    class function NowUtc: TDateTime; static;
  end;

{ TTimeZoneHelper }

class function TTimeZoneHelper.NowUtc: TDateTime;
begin
  Result := TTimeZone.Local.ToUniversalTime(Now);
end;


{ TAccessToken }

procedure TToken.Assign(Source: TPersistent);
var
  src: TToken absolute Source;
begin
  if src is TToken then
  begin
    fAccessToken := src.AccessToken;
    fRefreshToken := src.RefreshToken;
    fExpiryDate := src.ExpiryDate;
  end
  else
    inherited;
end;

function TToken.IsExpired: Boolean;
var
  current: TDateTime;
begin
  if fAccessToken.IsEmpty then
    raise EInvalidOpException.Create('Access  token is not defined');

  current := Now();
  Result := (current >= fExpiryDate) or (SecondsBetween(current, fExpiryDate) <= cExpiryTolerance);
end;

function TToken.IsValid: Boolean;
begin
  Result := not fAccessToken.IsEmpty and not IsExpired;
end;

procedure TToken.LoadFromFile(fileName: string);
var
  json: TJsonObject;
begin
  if fileName = '' then
    fileName := TPath.Combine(TPath.GetCachePath + TPath.DirectorySeparatorChar + cDefaultFileDir, cDefaultFileName);

  if not TFile.Exists(fileName) then
    Exit;

  json := TJsonObject.ParseFromFile(fileName) as TJsonObject;
  try
    json.ToSimpleObject(Self, False);
  finally
    json.Free;
  end;
end;

procedure TToken.SaveToFile(fileName: string);
var
  json: TJsonObject;
begin

  json := TJsonObject.Create;
  try
    json.FromSimpleObject(Self);
    if fileName = '' then
    begin
      fileName := TPath.GetCachePath + TPath.DirectorySeparatorChar + cDefaultFileDir;
      if not TDirectory.Exists(fileName) then
        TDirectory.CreateDirectory(fileName);
      fileName := TPath.Combine(fileName, cDefaultFileName);
    end;

    json.SaveToFile(fileName);
  finally
    json.Free;
  end;
end;

procedure TToken.SetExpiresIn(const Value: Cardinal);
begin
  if Value > 0 then
    fExpiryDate := IncSecond(Now, Value);
end;

procedure TToken.SetExpiryDateUtc(const Value: TDateTime);
begin
  fExpiryDate := TTimeZone.Local.ToUniversalTime(Value);
end;

function TToken.GetExpiryDateUtc: TDateTime;
begin
  Result := TTimeZone.Local.ToUniversalTime(fExpiryDate);
end;

end.
