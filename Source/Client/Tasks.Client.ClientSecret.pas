unit Tasks.Client.ClientSecret;

interface

type
  PClientSecrets = ^TClientSecrets;
  TClientSecrets = record
    ClientId: string;
    ClientSecret: string;
    function IsEmpty: Boolean;
    constructor Create(const id, secret: string);
    class function ReadFromCredentials(fileName: string): TClientSecrets; static;
  end;



implementation

uses
  JsonDataObjects,
  System.IOUtils;

{ TClientSecret }

function TClientSecrets.IsEmpty: Boolean;
begin
  Result := (ClientId = '') or (ClientSecret = ''); 
end;

constructor TClientSecrets.Create(const id, secret: string);
begin
  ClientId := id;
  ClientSecret := secret;  
end;

class function TClientSecrets.ReadFromCredentials(fileName: string):
  TClientSecrets;
var
  fileObj, json: TJsonObject;
begin
  if fileName = '' then
    fileName := TPath.Combine(TPath.GetLibraryPath, 'credentials.json');

  fileObj := TJsonObject.ParseFromFile(fileName) as TJsonObject;
  try
    json := fileObj.O['installed'];
    Result.ClientId := json.S['client_id'];
    Result.ClientSecret := json.S['client_secret'];
  finally
    fileObj.Free;
  end;
end;

end.
