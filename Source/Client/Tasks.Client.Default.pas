unit Tasks.Client.Default;

interface

uses
  Tasks.Client,
  Tasks.Client.Rest,
  Tasks.Client.RequestBuilder,
  Tasks.Client.ClientSecret,

  System.SysUtils,
  Spring
  ;

type
  TTasksServiceFactory = TFunc<IRequestBuilder, ITasksService>;
  TTaskListsServiceFactory = TFunc<IRequestBuilder, ITaskListsService>;

  TTasksClient = class(TInterfacedObject, ITasksClient)
  private
    const
      cBaseUrl = 'https://www.googleapis.com/tasks/v1';
      cAuthEndpoint = 'https://accounts.google.com/o/oauth2/v2/auth';
      cTokenEndpoint = 'https://www.googleapis.com/oauth2/v4/token';
      cRedirectEnpoint = 'http://127.0.0.1';
      cScope = 'https://www.googleapis.com/auth/tasks';
      cScopeReadOnly = cScope + '.readonly';
      cScopeToStr: array [TAccessScope] of string = (cScope, cScopeReadOnly);
  private
    fTaskListsFactory: TTaskListsServiceFactory;
    fTasksFacory: TTasksServiceFactory;
    fTasksService: ITasksService;
    fTaskListsService: ITaskListsService;
    fClient: IShared<TRestClient>;
  protected
    function GetTaskListsService: ITaskListsService;
    function GetTasksService: ITasksService;
    function Authorize(accessScope: TAccessScope): Boolean;
  public
    constructor Create(
      const tasksFactory: TTasksServiceFactory;
      const taskListsFactory: TTaskListsServiceFactory
    ); overload;
    constructor Create(
      const tasksFactory: TTasksServiceFactory;
      const taskListsFactory: TTaskListsServiceFactory;
      const secret: TClientSecrets
    ); overload;
    destructor Destroy; override;
  end;

implementation

uses
  Tasks.Client.RequestBuilder.Default,
//  Tasks.Client.Rest.OAuth2,
  Tasks.Client.Rest.OAuth2.Win
//  Spring
  ;

{ TTasksClient }


function TTasksClient.Authorize(accessScope: TAccessScope): Boolean;
begin
  fClient.Auth.Scope := cScopeToStr[accessScope];
  Result := fClient.Auth.Authorize;
end;

constructor TTasksClient.Create(const tasksFactory: TTasksServiceFactory;
  const taskListsFactory: TTaskListsServiceFactory; const secret: TClientSecrets);
var
  auth: TOAuth2Client;
begin
  Guard.CheckNotNull<TTasksServiceFactory>(tasksFactory, 'tasksFactory');
  Guard.CheckNotNull<TTaskListsServiceFactory>(taskListsFactory, 'taskListsFactory');
  Guard.CheckNotNull(secret, 'secret');
  Guard.CheckFalse(secret.IsEmpty, 'Client secret is empty');

  fTasksFacory := tasksFactory;
  fTaskListsFactory := taskListsFactory;

  auth := TOAuth2ClientWin.Create(secret);
  auth.AuthorizationEndpoint := cAuthEndpoint;
  auth.AccessTokenEndpoint := cTokenEndpoint;
  auth.RedirectionEndpoint := cRedirectEnpoint;
  auth.Scope := cScope;
  auth.TokenType := TOAuth2TokenType.ttBEARER;

  fClient := Shared.Make<TRestClient>(TRestClient.Create(auth));
  fClient.BaseURL := cBaseUrl;
end;

constructor TTasksClient.Create(const tasksFactory: TTasksServiceFactory; 
  const taskListsFactory: TTaskListsServiceFactory);
begin
  raise ENotImplementedException.Create('TTasksClient.Create: constructor not implemented') ;
end;

destructor TTasksClient.Destroy;
begin
  fClient := nil;
  inherited;
end;
function TTasksClient.GetTaskListsService: ITaskListsService;
begin
  {TODO -oOwner -cGeneral : Create transient instances}
  if not Assigned(fTaskListsService) then
    fTaskListsService := fTaskListsFactory(TRequestBuilder.Produce(fClient));
  Result := fTaskListsService;
end;

function TTasksClient.GetTasksService: ITasksService;
begin
  {TODO -oOwner -cGeneral : Create transient instances}
  if not Assigned(fTasksService) then
    fTasksService := fTasksFacory(TRequestBuilder.Produce(fClient));
  Result := fTasksService;
end;


end.
