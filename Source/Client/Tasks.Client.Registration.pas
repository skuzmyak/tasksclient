unit Tasks.Client.Registration;

interface

uses
  Tasks.Client,
  Tasks.Client.Default;

type
  TTasksClientRegistrator = class
  public
    class function TaskListsServiceFactory: TTaskListsServiceFactory;
    class function TasksServiceFactory: TTasksServiceFactory;
    class function TasksClientResolver: TasksClient.TResolver;
  public
    class procedure BindDefaultResolver; static;
  end;

implementation

uses
  Tasks.Client.Service.Tasks,
  Tasks.Client.Service.TaskLists,
  Tasks.Client.RequestBuilder;



class function TTasksClientRegistrator.TasksClientResolver: TasksClient.TResolver;
begin
  Result := function (const secret: TClientSecrets): ITasksClient
            begin
              Result := TTasksClient.Create(
                TTasksClientRegistrator.TasksServiceFactory(),
                TTasksClientRegistrator.TaskListsServiceFactory(),
                secret
              );
            end;
end;

class function TTasksClientRegistrator.TaskListsServiceFactory: TTaskListsServiceFactory;
begin
  Result := function (builder: IRequestBuilder): ITaskListsService
            begin
              Result := TTaskListsService.Create(builder);
            end;
end;


class function TTasksClientRegistrator.TasksServiceFactory: TTasksServiceFactory;
begin
  Result := function (builder: IRequestBuilder): ITasksService
            begin
              Result := TTasksService.Create(builder);
            end;
end;

class procedure TTasksClientRegistrator.BindDefaultResolver;
begin
  TasksClient._resolver := TTasksClientRegistrator.TasksClientResolver();  
end;

end.

