unit Tasks.Client.RequestBuilder.Default;

interface

uses
  Tasks.Client.RequestBuilder,
  Tasks.Client.Rest,
  Spring;

type
  TRequestBuilder = class;
  TRequestBuilderClass = class of TRequestBuilder;

  TRequestBuilder = class abstract(TInterfacedObject, IRequestBuilderTransient, IRequestBuilder)
  private
    function IRequestBuilder.GET = GetBuilder;
    function IRequestBuilder.PUT = PutBuilder;
    function IRequestBuilder.POST = PostBuilder;
    function IRequestBuilder.DELETE = DeleteBuilder;
    function GetBuilder: IRequestBuilderTransient;
    function PutBuilder: IRequestBuilderTransient;
    function PostBuilder: IRequestBuilderTransient;
    function DeleteBuilder: IRequestBuilderTransient;
    function Path(const resourcePath: string): IRequestBuilderTransient;
    function AddPathParam(const name: string; const value: string): IRequestBuilderTransient;
    function AddQueryParam(const name: string; const value: string): IRequestBuilderTransient; overload;
    function AddQueryParam(const name: string; value: Int64): IRequestBuilderTransient; overload;
    function AddQueryParam(const name: string; value: TDateTime): IRequestBuilderTransient; overload;
    function AddQueryParam(const name: string; value: Boolean): IRequestBuilderTransient; overload;
    function AddBody(const body: string): IRequestBuilderTransient;
    function Build: IShared<TRestRequest>;
  private
    [weak] fClient: TRestClient;
    fSessionInstance: IShared<TRestRequest>;
  protected
    constructor CreateInternal(client: TRestClient); virtual;

    function SetSessionIntance(method: TRestMethod): IRequestBuilder; virtual;
    function GetSessionInstance: TRestRequest; virtual;
  protected
    class var fDefaultClass: TRequestBuilderClass;
    class function ConstructInstance(client: TRestClient): IRequestBuilder; virtual;
  public
    constructor Create;
    destructor Destroy; override;
    class function Produce(client: TRestClient): IRequestBuilder; static;

    class function GET(client: TRestClient): IRequestBuilderTransient; static;
    class function PUT(client: TRestClient): IRequestBuilderTransient; static;
    class function POST(client: TRestClient): IRequestBuilderTransient; static;
    class function DELETE(client: TRestClient): IRequestBuilderTransient; static;
  end;

implementation

uses
  System.SysUtils,
  System.DateUtils;
//  Tasks.Types.Serializer.Manual;

type
  TRequestBuilderImpl = class(TRequestBuilder);

{ TRequestBuilder }

function TRequestBuilder.GetBuilder: IRequestBuilderTransient;
begin
  Result := SetSessionIntance(TRestMethod.rmGET);
end;

function TRequestBuilder.PutBuilder: IRequestBuilderTransient;
begin
  Result := SetSessionIntance(TRestMethod.rmPUT);
end;

function TRequestBuilder.PostBuilder: IRequestBuilderTransient;
begin
  Result := SetSessionIntance(TRestMethod.rmPOST);
end;

function TRequestBuilder.DeleteBuilder: IRequestBuilderTransient;
begin
  Result := SetSessionIntance(TRestMethod.rmDELETE);
end;

destructor TRequestBuilder.Destroy;
begin

  inherited;
end;

function TRequestBuilder.Path(const resourcePath: string): IRequestBuilderTransient;
begin
  GetSessionInstance.Resource := resourcePath;
  Result := Self;
end;

function TRequestBuilder.AddPathParam(const name: string; const value: string): IRequestBuilderTransient;
begin
  if not value.IsEmpty then
    GetSessionInstance.AddParameter(name, value, TRequestParamKind.pkURLSEGMENT);
  Result := Self;
end;

function TRequestBuilder.AddQueryParam(const name: string;
  value: TDateTime): IRequestBuilderTransient;
begin
  if value > 0 then
    AddQueryParam(name, DateToISO8601(value, False));
  Result := Self;
end;

function TRequestBuilder.AddQueryParam(const name: string; value: Int64): IRequestBuilderTransient;
begin
  AddQueryParam(name, value.ToString);
  Result := Self;
end;

function TRequestBuilder.AddQueryParam(const name: string; const value: string): IRequestBuilderTransient;
begin
  if not value.IsEmpty then
    GetSessionInstance.AddParameter(name, value, TRequestParamKind.pkGETorPOST);
  Result := Self;
end;

function TRequestBuilder.AddBody(const body: string): IRequestBuilderTransient;
begin
  GetSessionInstance
    .Params
    .AddItem('body', body, TRequestParamKind.pkREQUESTBODY, [], TRestContentType.ctAPPLICATION_JSON);
  Result := Self;
end;

function TRequestBuilder.Build: IShared<TRestRequest>;
begin
  if not Assigned(fSessionInstance) then
    raise EInvalidOpException.Create('Error building request: nothing to build, no session intance');
  Result := fSessionInstance;
  fSessionInstance := nil;
end;

function TRequestBuilder.SetSessionIntance(method: TRestMethod): IRequestBuilder;
begin
  if Assigned(fSessionInstance) then
    raise EInvalidOpException.Create('Error building request: there is already a pending request');
  fSessionInstance := Shared.Make<TRestRequest>(TRestRequest.Create(nil));
  fSessionInstance.Method := method;
  fSessionInstance.AutoCreateParams := False;
  fSessionInstance.Client := fClient;
  Result := Self;
end;

function TRequestBuilder.GetSessionInstance: TRestRequest;
begin
  if not Assigned(fSessionInstance) then
    raise EInvalidOpException.Create('Error building request: no pending request');
  Result := fSessionInstance();
end;

class function TRequestBuilder.ConstructInstance(client: TRestClient): IRequestBuilder;
var
  builderClass: TRequestBuilderClass;
begin
  builderClass := fDefaultClass;
  if not Assigned(builderClass) then
    builderClass := TRequestBuilderImpl;

  Result := builderClass.CreateInternal(client);
end;

constructor TRequestBuilder.CreateInternal(client: TRestClient);
begin
//  Guard.CheckNotNull(client, 'client');
  fClient := client;
end;

constructor TRequestBuilder.Create;
begin
  raise ENoConstructException.Create('TRequestBuilder: Use class methods method instead');
end;

class function TRequestBuilder.Produce(client: TRestClient): IRequestBuilder;
begin
  Result := ConstructInstance(client);
end;

class function TRequestBuilder.GET(client: TRestClient): IRequestBuilderTransient;
begin
  Result := TRequestBuilder.Produce(client).GET;
end;

class function TRequestBuilder.PUT(client: TRestClient): IRequestBuilderTransient;
begin
  Result := TRequestBuilder.Produce(client).PUT;
end;

class function TRequestBuilder.POST(client: TRestClient): IRequestBuilderTransient;
begin
  Result := TRequestBuilder.Produce(client).POST;
end;

class function TRequestBuilder.DELETE(client: TRestClient): IRequestBuilderTransient;
begin
  Result := TRequestBuilder.Produce(client).DELETE;
end;

function TRequestBuilder.AddQueryParam(const name: string;
  value: Boolean): IRequestBuilderTransient;
const
  BoolToStr: array [Boolean] of string = ('false', 'true'); //do not localize
begin
  AddQueryParam(name, BoolToStr[value])
end;

end.

