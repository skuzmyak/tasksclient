unit Tasks.Client.RequestBuilder;

interface

uses
  Tasks.Client.Rest,
  Spring;

type
  IRequestBuilderTransient = interface(IInvokable)
  ['{BA23848B-EEC4-4596-932C-A977B9897762}']
    function Path(const resourcePath: string): IRequestBuilderTransient;
    function AddPathParam(const name, value: string): IRequestBuilderTransient;
    function AddBody(const body: string): IRequestBuilderTransient;

    function AddQueryParam(const name, value: string): IRequestBuilderTransient; overload;
    function AddQueryParam(const name: string; value: Int64): IRequestBuilderTransient; overload;
    /// <summary> input value expected to be NON UTC </summary>
    function AddQueryParam(const name: string; value: TDateTime): IRequestBuilderTransient; overload;
    function AddQueryParam(const name: string; value: Boolean): IRequestBuilderTransient; overload;

    function Build: IShared<TRestRequest>;
  end;

  IRequestBuilder = interface(IRequestBuilderTransient)
  ['{D2C590A1-AA6B-46EA-B63D-152FE5FDE8CF}']
    function GET: IRequestBuilderTransient;
    function PUT: IRequestBuilderTransient;
    function POST: IRequestBuilderTransient;
    function DELETE: IRequestBuilderTransient;
  end;

implementation

end.
