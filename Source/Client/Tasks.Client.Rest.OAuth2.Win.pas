unit Tasks.Client.Rest.OAuth2.Win;

interface

uses
  Tasks.Client.Rest,
  System.Classes;

type
  TOnWaitForRequestEvent = procedure(
    Sender: TObject;
    Attempt: Cardinal;
    var Timeout: Cardinal;
    var StopWaiting: Boolean
  ) of object;

  TOAuth2ClientWin = class(TOAuth2Client)
  private const
    cLoopbackIp = '127.0.0.1';
    cDefaultWaitTimeout = 10 * 1000; //10 sec
  private
    fServer: TThread;
    fOnWaitForRequest: TOnWaitForRequestEvent;
    procedure StartServer;
  protected
    procedure DoWaitForRequest(Attempt: Cardinal; var Timeout: Cardinal; var StopWaiting: Boolean); virtual;
    procedure FetchAuthCode; override;
  public
    destructor Destroy; override;
    /// <summary>
    ///   Terminates listener http thread
    /// </summary>
    procedure StopServer;
    /// <summary>
    ///   Callback while waiting for user input
    ///  Adds ability to control how much we should wait for request
    /// </summary>
    property OnWaitForRequest: TOnWaitForRequestEvent read fOnWaitForRequest write fOnWaitForRequest;
  end;

implementation

uses
  System.SyncObjs,
  System.SysUtils,

  IdContext,
  IdCustomHTTPServer,
  IdHTTPServer,

  Winapi.ShellAPI,
  Winapi.Windows;

type
  /// <summary>
  ///   Http server which listens on random port for
  ///  auth code.
  /// </summary>
  TOAuth2HttpListener = class(TThread)
  private const
    cDefaultPort = 9004;
  private
    fLoopbackIp: string;
    fDoneEvent: TEvent;
    fServerActiveEvent: TEvent;
    fServer: TIdHTTPServer;
    fCode: string;
    fError: string;
    procedure CommandGetEvent(AContext: TIdContext; ARequestInfo: TIdHTTPRequestInfo;
      AResponseInfo: TIdHTTPResponseInfo);
    procedure ConnectEvent(AContext: TIdContext);
    procedure DisconnectEvent(AContext: TIdContext);
  protected
    constructor Create(const loopbackIp: string);
    destructor Destroy; override;
    procedure Execute; override;
    procedure TerminatedSet; override;
    /// <summary>
    ///   May block calling thread for 0.5 sec if it
    ///  still looking for a free port.
    ///  Usually returns immediately.
    /// </summary>
    function WaitForActiveServer(out selectedPort: Cardinal): Boolean;
    /// <summary>
    ///   Waits for user to decide weather to give access or not.
    ///   Block calling thread for a given timeout
    /// </summary>
    function WaitForRequest(timeout: Cardinal): Boolean;
    /// <summary>
    ///   the same as WaitForRequest but return auth code in case
    ///  access was given
    /// </summary>
    function WaitForAuthCode(out authCode: string; timeout: Cardinal = INFINITE): Boolean;
    property Code: string read fCode;
    property Error: string read fError;
  end;

const
  cResponseAccessGranted =
     '<html>'
    +'    <head>'
    +'        <title>Access granted</title>'
    +'    </head>'
    +'    <body>'
    +'        <h2 style="text-align:center">Authentication completed</h2>'
    +'        <h3 style="text-align:center">Please, return to your app</h3>'
    +'    </body>'
    +'</html>';

  cResponseAccessDenied =
     '<html>'
    +'    <head>'
    +'        <title>Access denied</title>'
    +'    </head>'
    +'    <body>'
    +'        <h3 style="text-align:center">Authentication failed</h3>'
    +'    </body>'
    +'</html>';


{ TOAuth2HttpListener }

procedure TOAuth2HttpListener.CommandGetEvent(AContext: TIdContext; ARequestInfo: TIdHTTPRequestInfo;
  AResponseInfo: TIdHTTPResponseInfo);
begin
  if fDoneEvent.WaitFor(0) = wrSignaled then
    Exit;

  fCode := ARequestInfo.Params.Values['code'];
  if fCode <> '' then
    AResponseInfo.ContentText := cResponseAccessGranted
  else
  begin
    fError := ARequestInfo.Params.Values['error'];
    if fError <> '' then //it is assumed that error could be only 'access_denied'
      AResponseInfo.ContentText := cResponseAccessDenied
  end;
  
  if (fCode <> '') or (fError <> '') then
    fDoneEvent.SetEvent
  else
    raise EIdHTTPErrorParsingCommand.Create('Unsupported request');
end;

procedure TOAuth2HttpListener.ConnectEvent(AContext: TIdContext);
begin

end;

procedure TOAuth2HttpListener.DisconnectEvent(AContext: TIdContext);
begin

end;

constructor TOAuth2HttpListener.Create(const loopbackIp: string);
begin
  if loopbackIp = '' then
    raise EIdHTTPServerError.Create('LoopBack IP adress must be defined');

  inherited Create(False);
  fLoopbackIp := loopbackIp;
  fDoneEvent := TEvent.Create();
  fServerActiveEvent := TEvent.Create();
  fServer := TIdHTTPServer.Create(nil);
  fServer.OnCommandGet := CommandGetEvent;
  fServer.OnConnect := ConnectEvent;
  fServer.OnDisconnect := DisconnectEvent;
end;

procedure TOAuth2HttpListener.Execute;
const
  cAttemptCount = 50;
var
  port: Cardinal;
begin
  //find available port
  port := cDefaultPort;
  while not fServer.Active and (port - cDefaultPort <= cAttemptCount) do
    try
      fServer.Bindings.Clear;
      fServer.Bindings.Add.SetBinding(fLoopbackIp, port);
      fServer.DefaultPort := port;
      fServer.Active := True;

      fServerActiveEvent.SetEvent;
      Break;
    except
      Inc(port);
    end;

  if not fServer.Active then
  begin
    fDoneEvent.SetEvent;
    Exit;
  end;

  if Terminated then
    Exit;

  if fDoneEvent.WaitFor() = wrSignaled then
    Exit; //that's all - wait for request or terminate it externally
end;

procedure TOAuth2HttpListener.TerminatedSet;
begin
  inherited;
  fDoneEvent.SetEvent;
end;

function TOAuth2HttpListener.WaitForActiveServer(out selectedPort: Cardinal): Boolean;
begin
  Result := fServerActiveEvent.WaitFor(500) = wrSignaled;
  if Result then
     selectedPort := fServer.DefaultPort;
end;

function TOAuth2HttpListener.WaitForAuthCode(out authCode: string; timeout: Cardinal = INFINITE): Boolean;
begin
  Result := WaitForRequest(timeOut) and (fCode <> '');
  if Result then
    authCode := fCode;
end;

function TOAuth2HttpListener.WaitForRequest(timeout: Cardinal): Boolean;
begin
  Result := fDoneEvent.WaitFor(timeout) = wrSignaled;
end;

destructor TOAuth2HttpListener.Destroy;
begin
  fServer.Active := False;
  fServer.Free;
  fDoneEvent.Free;
  fServerActiveEvent.Free;
  inherited;  
end;

{ TOAuth2ClientWin }

destructor TOAuth2ClientWin.Destroy;
begin
  StopServer;
  inherited;
end;

procedure TOAuth2ClientWin.DoWaitForRequest(Attempt: Cardinal; var Timeout: Cardinal;
  var StopWaiting: Boolean);
begin
  if Assigned(fOnWaitForRequest) then
    fOnWaitForRequest(Self, Attempt, Timeout, StopWaiting);
end;

procedure TOAuth2ClientWin.FetchAuthCode;
var
  port: Cardinal;
  timeout: Cardinal;
  attemptCount: Cardinal;
  server: TOAuth2HttpListener;
  stop: Boolean;
begin
  StartServer;
  try
    server := fServer as TOAuth2HttpListener;
    if not server.WaitForActiveServer(port) then
      raise EIdHTTPServerError.Create('Unable to activate http sevrer');

    Self.RedirectionEndpoint := Self.RedirectionEndpoint + ':' + port.ToString;
    //now open default browser
    ShellExecute(0, 'open', PChar(Self.AuthorizationRequestURI), nil, nil, SW_SHOWNORMAL);

    timeout := cDefaultWaitTimeout;
    attemptCount := 1;
    stop := False;
    DoWaitForRequest(attemptCount, timeout, stop);
    
    while not stop and not server.WaitForRequest(timeout) do
    begin
      Inc(attemptCount);
      DoWaitForRequest(timeout, attemptCount, stop);
    end;

    Self.AuthCode := server.Code;
  finally
    StopServer;
  end;
end;

procedure TOAuth2ClientWin.StartServer;
begin
  Assert(fServer = nil);
  fServer := TOAuth2HttpListener.Create(cLoopbackIp);
end;

procedure TOAuth2ClientWin.StopServer;
begin
  if fServer <> nil then
  begin
    fServer.Terminate;
    fServer.WaitFor;
    FreeAndNil(fServer);
  end;
end;

end.
