unit Tasks.Client.Rest;

interface

uses
  REST.Types,
  REST.Client,
  REST.Authenticator.OAuth,

  Tasks.Client.AccessToken,
  Tasks.Client.ClientSecret,

  Spring
;

type
  TRestMethod = REST.Types.TRESTRequestMethod;
  TRequestParamKind = REST.Types.TRESTRequestParameterKind;
  TRequestParamOption = REST.Types.TRESTRequestParameterOption;
  TRestContentType = REST.Types.TRESTContentType;

  TCustomRestClient = REST.Client.TCustomRESTClient;
  TCustomAuthenticator = REST.Client.TCustomAuthenticator;
  TOAuth2TokenType = REST.Authenticator.OAuth.TOAuth2TokenType;

  TOAuth2Client = class;

  TRestClient = class(TCustomRESTClient)
  private
    fAuth: IShared<TOAuth2Client>;
    function GetAuth: TOAuth2Client;
  public
    constructor Create(const auth: Shared<TOAuth2Client>); reintroduce;
    property Auth: TOAuth2Client read GetAuth;
  end;

  TOAuth2Client = class abstract(TOAuth2Authenticator)
  private const
    cParamRefreshToken = TToken.cPropRefreshToken;
    cParamAccessToken = TToken.cPropAccessToken;
    cParamExpiresIn = TToken.cPropExpiresIn;
    cParamClientId = 'client_id';
    cParamClientSecret = 'client_secret';
    cParamGrantType = 'grant_type'; 
  private
    fDefaultToken: IShared<TToken>;
    fToken: IShared<TToken>;
    function GetToken: IShared<TToken>;
    procedure SetToken(const Value: IShared<TToken>);
  protected
    procedure RefreshAccessToken;
    procedure FetchAccessToken;
    procedure FetchAuthCode; virtual; abstract;
    procedure DoAuthenticate(ARequest: TCustomRESTRequest); override;
  public
    constructor Create(const secret: TClientSecrets); reintroduce; overload;
    function Authorize: Boolean;
    property Token: IShared<TToken> read GetToken write SetToken;
  end;

  TRestResponse = class(TCustomRESTResponse);
  
  TRestRequest = class(TCustomRESTRequest)
  public
    constructor Create; reintroduce; overload;
    destructor Destroy; override;
    procedure AfterConstruction; override;
    function Execute: TRestResponse; reintroduce; virtual;
  end;

implementation

uses
  IPPeerClient,
  JsonDataObjects;

type
  TTokenHelper = class helper for TToken
  public
    procedure AssignFrom(value: TOAuth2Client);
    procedure AssignTo(value: TOAuth2Client);
    procedure ParseFromJson(const source: string);
  end;

{ TTokenHelper }

procedure TTokenHelper.AssignFrom(value: TOAuth2Client);
begin
  AccessToken := value.AccessToken;
  RefreshToken := value.RefreshToken;
  ExpiryDate := value.AccessTokenExpiry;
end;

procedure TTokenHelper.AssignTo(value: TOAuth2Client);
begin
  value.AccessToken := AccessToken;
  value.RefreshToken := RefreshToken;
  value.AccessTokenExpiry := ExpiryDate;
end;


procedure TTokenHelper.ParseFromJson(const source: string);
var
  json: Shared<TJsonObject>;
begin
  json := TJsonObject.Parse(source) as TJsonObject;
  Self.AccessToken := json.Value.S[cPropAccessToken];
  Self.ExpiresIn := json.Value.I[cPropExpiresIn];
end;

{ TRestClient }

constructor TRestClient.Create(const auth: Shared<TOAuth2Client>);
begin
  fAuth := auth;
  Guard.CheckNotNull(fAuth(), 'authenticator');
  inherited Create(nil);
  Authenticator := fAuth();
end;

function TRestClient.GetAuth: TOAuth2Client;
begin
  Result := fAuth();
end;

{ TRestRequest }

procedure TRestRequest.AfterConstruction;
begin
  Self.Response := TRestResponse.Create(Self);
end;

function TRestRequest.Execute: TRestResponse;
begin
  inherited Execute;
  Result := Self.Response as TRestResponse;
end;

destructor TRestRequest.Destroy;
begin
  
  inherited;  
end;

constructor TRestRequest.Create;
begin
  inherited Create(nil);  
end;

{ TOAuth2Client }

procedure TOAuth2Client.DoAuthenticate(ARequest: TCustomRESTRequest);
begin
  if (ClientID = '') or (ClientSecret = '') then
    raise EOAuth2Exception.Create('Client secret and client id must be defined');

  //enough to check refresh token
  if Token.RefreshToken = '' then
  begin
    FetchAuthCode;
    FetchAccessToken;
  end 
  else
    RefreshAccessToken;

  inherited DoAuthenticate(ARequest);  
end;

procedure TOAuth2Client.RefreshAccessToken;
var
  client: Shared<TCustomRestClient>;
  req: TRestRequest;
  res: TRestResponse;
begin
  if not Token.IsExpired then
    Exit; 

  client := TCustomRestClient.Create(Self.AccessTokenEndpoint);
  req := TRestRequest.Create(client.Value);
  req.Method := TRestMethod.rmPOST;
  
  req.AddAuthParameter(cParamRefreshToken, Token.RefreshToken, pkGETorPOST);
  req.AddAuthParameter(cParamClientId, Self.ClientID, pkGETorPOST);
  req.AddAuthParameter(cParamClientSecret, Self.ClientSecret, pkGETorPOST);
  req.AddAuthParameter(cParamGrantType, 'refresh_token', pkGETorPOST);

  res := req.Execute;
  if not res.Status.Success then
    raise EOAuth2Exception.Create('failed to get refresh token');

  Token.ParseFromJson(res.Content);
  if not Token.IsValid then
    raise EOAuth2Exception.Create('Failed to refresh access token');

  Token.AssignTo(Self);
end;

procedure TOAuth2Client.SetToken(const Value: IShared<TToken>);
begin
  fToken := Value;
  if not Assigned(fToken) then
    Exit;

  fToken.AssignTo(Self);
end;

procedure TOAuth2Client.FetchAccessToken;
begin
  ChangeAuthCodeToAccesToken;
  Token.AssignFrom(Self);

  if not Token.IsValid then
    raise EOAuth2Exception.Create('Error while exchanging auth code to access token');
end;

function TOAuth2Client.GetToken: IShared<TToken>;
begin
  Result := fToken;
  if not Assigned(Result) then
  begin
    if not Assigned(fDefaultToken) then
    begin
      fDefaultToken := Shared.Make<TToken>(
        TToken.Create,
        procedure (const value: TToken)
        begin
          value.SaveToFile();
          value.Free;
        end
      );
      fDefaultToken.LoadFromFile();
      fDefaultToken.AssignTo(Self);      
    end;  

    Result := fDefaultToken;
  end;
end;

function TOAuth2Client.Authorize: Boolean;
begin
  Result := True;
  try
    FetchAuthCode;
    FetchAccessToken;
  except
    Result := False;
  end;
end;

constructor TOAuth2Client.Create(const secret: TClientSecrets);
begin
  inherited Create(nil);
  Self.ClientID := secret.ClientId;
  Self.ClientSecret := secret.ClientSecret;
end;

end.
