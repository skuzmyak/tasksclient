unit Tasks.Client.Service.Base;

interface

uses
  Tasks.Types.Serializer,
  Tasks.Client.RequestBuilder,
  Spring;

type
  TResourceService = class abstract (TInterfacedObject)
  strict private
    fRequestBuilder: IRequestBuilder; 
    fSerializer: TResourceSerializer;
    fDefaultSerializer: TResourceSerializer;
    function GetSerializer: TResourceSerializer;
  strict protected
    property Builder: IRequestBuilder read fRequestBuilder;
  public
    constructor Create(const requestBuilder: IRequestBuilder); virtual;
    destructor Destroy; override;
    property Serializer: TResourceSerializer read GetSerializer write fSerializer;
  end;

  TRequestParam = class
  public const
    /// <summary>
    ///   string Task list identifier.
    /// </summary>
    TaskId = 'task';
    /// <summary>
    ///   string Task list identifier.
    /// </summary>
    TaskListId   =  'tasklist';
    /// <summary>
    ///   string. Upper bound for a task's completion date (as a RFC 3339
    ///   timestamp) to filter by. Optional. The default is not to filter by
    ///   completion date.
    /// </summary>
    CompletedMax  = 'completedMax';
    /// <summary>
    ///   string. Lower bound for a task's completion date (as a RFC 3339
    ///   timestamp) to filter by. Optional. The default is not to filter by
    ///   completion date.
    /// </summary>
    CompletedMin  = 'completedMin';
    /// <summary>
    ///   string Upper bound for a task's due date (as a RFC 3339 timestamp) to
    ///   filter by. Optional. The default is not to filter by due date.
    /// </summary>
    DueMax        = 'dueMax';
    /// <summary>
    ///   string Lower bound for a task's due date (as a RFC 3339 timestamp) to
    ///   filter by. Optional. The default is not to filter by due date.
    /// </summary>
    DueMin        = 'dueMin';
    /// <summary>
    ///   long Maximum number of task lists returned on one page. Optional. The
    ///   default is 100.
    /// </summary>
    MaxResults    = 'maxResults';
    /// <summary>
    ///   string Token specifying the result page to return. Optional.
    /// </summary>
    PageToken     = 'pageToken';
    /// <summary>
    ///   boolean Flag indicating whether completed tasks are returned in the
    ///   result. Optional. The default is True.
    /// </summary>
    ShowCompleted = 'showCompleted';
    /// <summary>
    ///   boolean Flag indicating whether deleted tasks are returned in the
    ///   result. Optional. The default is False.
    /// </summary>
    ShowDeleted   = 'showDeleted';
    /// <summary>
    ///   boolean Flag indicating whether hidden tasks are returned in the
    ///   result. Optional. The default is False.
    /// </summary>
    ShowHidden    = 'showHidden';
    /// <summary>
    ///   string Lower bound for a task's last modification time (as a RFC 3339
    ///   timestamp) to filter by. Optional. The default is not to filter by
    ///   last modification time.
    /// </summary>
    UpdatedMin    = 'updatedMin';
    /// <summary>
    ///   string Parent task identifier. If the task is created/moved at the top
    ///   level, this parameter is omitted. Optional.
    /// </summary>
    ParentTaskId  = 'parent';
    /// <summary>
    ///   string Previous sibling task identifier. If the task is created/moved at
    ///   the first position among its siblings, this parameter is omitted.
    ///   Optional.
    /// </summary>
    PreviousTaskId = 'previous';
  end;

implementation

constructor TResourceService.Create(const requestBuilder: IRequestBuilder);
begin
  Guard.CheckNotNull(requestBuilder, 'requestBuilder');
  fRequestBuilder := requestBuilder;
  fDefaultSerializer := TResourceSerializer.CreateDefault;
end;

destructor TResourceService.Destroy;
begin
  fRequestBuilder := nil;
  fSerializer := nil;
  fDefaultSerializer.Free;
  inherited;  
end;

function TResourceService.GetSerializer: TResourceSerializer;
begin
  Result := fSerializer;
  if not Assigned(Result) then
    Result := fDefaultSerializer;
end;

end.
