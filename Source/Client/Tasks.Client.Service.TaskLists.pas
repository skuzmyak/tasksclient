unit Tasks.Client.Service.TaskLists;

interface

uses
  Tasks.Types,
  Tasks.Client,
  Tasks.Client.Service.Base;

type
  TTaskListsService = class(TResourceService, ITaskListsService)
  private type
    TPath = class
    private const  
      Base = 'users/@me/lists';
      List = Base;
      Get = Base + '/{' + TRequestParam.TaskListId + '}';
      Insert = Base;
      Update = Get;
      Delete = Get;
      Patch = Get;
    end;
  private
    function List: IGoogleTaskListCollection; overload;
    function List(maxResults: Int64; const pageToken: string): IGoogleTaskListCollection; overload;
    function Get(const taskListId: string): IGoogleTaskList;
    function Insert(const taskList: IGoogleTaskList): IGoogleTaskList;
    function Update(const taskList: IGoogleTaskList): IGoogleTaskList; overload;
    function Update(const taskListId: string; const body: IGoogleTaskList): IGoogleTaskList; overload;
    procedure Delete(const taskListId: string);
  end; 

implementation

function TTaskListsService.List: IGoogleTaskListCollection;
var
  res: string;
begin
  res := Builder.GET
    .Path(TPath.List)
    .Build
    .Execute
    .Content;

  Result := Serializer.Deserialize<IGoogleTaskListCollection>(res);  
end;

function TTaskListsService.List(maxResults: Int64; const pageToken: string):
  IGoogleTaskListCollection;
var
  res: string;
begin
  res := Builder.GET
    .Path(TPath.List)
    .AddQueryParam(TRequestParam.MaxResults, maxResults)
    .AddQueryParam(TRequestParam.PageToken, pageToken)
    .Build
    .Execute
    .Content;

  Result := Serializer.Deserialize<IGoogleTaskListCollection>(res);  
end;

function TTaskListsService.Get(const taskListId: string): IGoogleTaskList;
var
  res: string;
begin
  res := Builder.GET
    .Path(TPath.Get)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .Build
    .Execute
    .Content;

  Result := Serializer.Deserialize<IGoogleTaskList>(res);
end;

function TTaskListsService.Insert(const taskList: IGoogleTaskList): IGoogleTaskList;
var
  res: string;
begin
  res := Builder.POST
    .Path(TPath.Insert)
    .AddBody(Serializer.Serialize(taskList))
    .Build
    .Execute
    .Content;

  Result := Serializer.Deserialize<IGoogleTaskList>(res);  
end;

function TTaskListsService.Update(const taskListId: string;
  const body: IGoogleTaskList): IGoogleTaskList;
var
  res: string;
begin
  res := Builder.PUT
    .Path(TPath.Update)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .AddBody(Serializer.Serialize(body))
    .Build
    .Execute
    .Content;

  Result := Serializer.Deserialize<IGoogleTaskList>(res);
end;

function TTaskListsService.Update(const taskList: IGoogleTaskList): IGoogleTaskList;
begin
  Result := Update(taskList.Id, taskList);
end;

procedure TTaskListsService.Delete(const taskListId: string);
begin
  Builder.DELETE
    .Path(TPath.Delete)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .Build
    .Execute
    .Content;  
end;

end.
