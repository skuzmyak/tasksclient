unit Tasks.Client.Service.Tasks;

interface

uses
  Tasks.Types,
  Tasks.Client,
  Tasks.Client.Service.Base
  ;

type
  TTasksService = class(TResourceService, ITasksService)
  protected type
    TPath = class
    public const
      Base = 'lists/{' + TRequestParam.TaskListId + '}/';
      List = Base + 'tasks';
      Get = List + '/{' + TRequestParam.TaskId  + '}';
      Insert = List;
      Update = Get;
      Delete = Get;
      Clear = Base + 'clear';
      Move = Get + '/move';
      Patch = Get;
    end;
  private
//    <ITasksResource>
    function List(const taskListId: string): IGoogleTaskCollection; overload;
    function List(
      const taskListId: string;
      completedMax: TDateTime;
      completedMin: TDateTime;
      dueMax: TDateTime;
      dueMin: TDateTime;
      updatedMin: TDateTime;
      maxResults: Int64 = 100;
      pageToken: string = '';
      showCompleted: Boolean = True;
      showDeleted: Boolean = False;
      showHidden: Boolean = False
    ): IGoogleTaskCollection; overload;
    function Get(const taskId, taskListId: string): IGoogleTask;
    function Insert(const task: IGoogleTask; const taskListId: string): IGoogleTask;
    function Update(const task: IGoogleTask; const taskListId: string): IGoogleTask; overload;
    function Update(const taskId, taskListId: string; const body: IGoogleTask): IGoogleTask; overload;
    procedure Delete(const taskId, taskListId: string);
    procedure Clear(const taskListId: string);
    function Move(const taskId, taskListId: string): IGoogleTask; overload;
    function Move(const taskId, taskListId: string;
      const parentTaskId, previousTaskId: string): IGoogleTask; overload;
//    </ITasksResource>
  end;

implementation

uses
  Tasks.Client.Rest,
  Spring;

{ TTasksResource }

function TTasksService.List(const taskListId: string): IGoogleTaskCollection;
var
  res: TRestResponse;
begin
  res := Builder.GET
    .Path(TPath.List)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .Build
    .Execute;

   Result := Serializer.Deserialize<IGoogleTaskCollection>(res.Content);
end;

function TTasksService.List(const taskListId: string; completedMax, completedMin, dueMax, dueMin,
  updatedMin: TDateTime; maxResults: Int64; pageToken: string; showCompleted, showDeleted,
  showHidden: Boolean): IGoogleTaskCollection;
var
  res: TRestResponse;
begin
  res := Builder.GET
    .Path(TPath.List)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .AddQueryParam(TRequestParam.CompletedMax, completedMax)
    .AddQueryParam(TRequestParam.CompletedMin, completedMin)
    .AddQueryParam(TRequestParam.DueMax, dueMax)
    .AddQueryParam(TRequestParam.DueMin, dueMin)
    .AddQueryParam(TRequestParam.UpdatedMin, updatedMin)
    .AddQueryParam(TRequestParam.MaxResults, maxResults)
    .AddQueryParam(TRequestParam.PageToken, pageToken)
    .AddQueryParam(TRequestParam.ShowCompleted, showCompleted)
    .AddQueryParam(TRequestParam.ShowDeleted, showDeleted)
    .AddQueryParam(TRequestParam.ShowHidden, showHidden)
    .Build
    .Execute;

  Result := Serializer.Deserialize<IGoogleTaskCollection>(res.Content);
end;

function TTasksService.Get(const taskId, taskListId: string): IGoogleTask;
var
  req: IShared<TRestRequest>;
  res: TRestResponse;
begin
  req := Builder.GET
    .Path(TPath.Get)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .AddPathParam(TRequestParam.TaskId, taskId)
    .Build;

  res := req.Execute;
  Result := Serializer.Deserialize<IGoogleTask>(res.Content);
end;

function TTasksService.Insert(const task: IGoogleTask; const taskListId: string): IGoogleTask;
var
  res: TRestResponse;
begin
  res := Builder.POST
    .Path(TPath.Insert)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .AddBody(Serializer.Serialize(task))
    .Build
    .Execute;

  Result := Serializer.Deserialize<IGoogleTask>(res.Content);
end;

function TTasksService.Update(const task: IGoogleTask; const taskListId: string): IGoogleTask;
begin
  Result := Update(task.Id, taskListId, task);
end;

function TTasksService.Update(const taskId, taskListId: string;
  const body: IGoogleTask): IGoogleTask;
var
  res: TRestResponse;
begin
  res := Builder.PUT
    .Path(TPath.Update)
    .AddPathParam(TRequestParam.TaskId, taskId)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .AddBody(Serializer.Serialize(body))
    .Build
    .Execute;

  Result := Serializer.Deserialize<IGoogleTask>(res.Content);
end;

procedure TTasksService.Delete(const taskId, taskListId: string);
begin
  Builder.DELETE
    .Path(TPath.Delete)
    .AddPathParam(TRequestParam.TaskId, taskId)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .Build
    .Execute;
end;

procedure TTasksService.Clear(const taskListId: string);
begin
  Builder.POST
    .Path(TPath.Clear)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .Build
    .Execute;
end;

function TTasksService.Move(const taskId, taskListId: string): IGoogleTask;
var
  res: TRestResponse;
begin
  res := Builder.POST
    .Path(TPath.Move)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .AddPathParam(TRequestParam.TaskId, taskId)
    .Build
    .Execute;

  Result := Serializer.Deserialize<IGoogleTask>(res.Content);
end;

function TTasksService.Move(const taskId, taskListId, parentTaskId,
  previousTaskId: string): IGoogleTask;
var
  res: TRestResponse;
begin
  res := Builder.POST
    .Path(TPath.Move)
    .AddPathParam(TRequestParam.TaskListId, taskListId)
    .AddPathParam(TRequestParam.TaskId, taskId)
    .AddQueryParam(TRequestParam.ParentTaskId, parentTaskId)
    .AddQueryParam(TRequestParam.PreviousTaskId, previousTaskId)
    .Build
    .Execute;

  Result := Serializer.Deserialize<IGoogleTask>(res.Content);
end;

end.
