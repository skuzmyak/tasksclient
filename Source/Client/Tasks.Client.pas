unit Tasks.Client;

interface

uses
  Tasks.Types,
  Tasks.Client.ClientSecret;

type
  ITasksService = interface;
  ITaskListsService = interface;
  TAccessScope = (ReadWrite, ReadOnly);
  TClientSecrets = Tasks.Client.ClientSecret.TClientSecrets;

  ITasksClient = interface(IInterface)
  ['{1CBA16DA-C706-4FFB-9A69-567AB188D5A8}']
    function Authorize(accessScope: TAccessScope): Boolean;

    function GetTasksService: ITasksService;
    function GetTaskListsService: ITaskListsService;

    property TaskListsService: ITaskListsService read GetTaskListsService;
    property TasksService: ITasksService read GetTasksService;
  end;

  ITaskListsService = interface(IInterface)
  ['{17D0B26B-A139-457A-B7F6-616EDB237630}']
    function List: IGoogleTaskListCollection; overload;
    function List(maxResults: Int64; const pageToken: string): IGoogleTaskListCollection; overload;
    function Get(const taskListId: string): IGoogleTaskList;
    function Insert(const taskList: IGoogleTaskList): IGoogleTaskList;
    function Update(const taskList: IGoogleTaskList): IGoogleTaskList; overload;
    function Update(const taskListId: string; const body: IGoogleTaskList): IGoogleTaskList; overload;
    procedure Delete(const taskListId: string);
//    function Patch(const ATaskListID: string): IResourceRequest;
  end;

  ITasksService = interface(IInterface)
  ['{5485C135-113A-4469-81BE-C9CD070DA9E9}']
    function List(const taskListId: string): IGoogleTaskCollection; overload;
    function List(
      const taskListId: string;
      completedMax: TDateTime;
      completedMin: TDateTime;
      dueMax: TDateTime;
      dueMin: TDateTime;
      updatedMin: TDateTime;
      maxResults: Int64 = 100;
      pageToken: string = '';
      showCompleted: Boolean = True;
      showDeleted: Boolean = False;
      showHidden: Boolean = False
    ): IGoogleTaskCollection; overload;
    function Get(const taskId, taskListId: string): IGoogleTask;
    function Insert(const task: IGoogleTask; const taskListId: string): IGoogleTask;
    function Update(const task: IGoogleTask; const taskListId: string): IGoogleTask; overload;
    function Update(const taskId, taskListId: string; const body: IGoogleTask): IGoogleTask; overload;
    procedure Delete(const taskId, taskListId: string);
    procedure Clear(const taskListId: string);
    function Move(const taskId, taskListId: string): IGoogleTask; overload;
    function Move(const taskId, taskListId: string;
      const parentTaskId, previousTaskId: string): IGoogleTask; overload;
//    function Patch(const ATask: IGoogleTask; const ATaskListID: string): IGoogleTask;
  end;

  TasksClient = record
  public type
    TResolver = reference to function(const secret: TClientSecrets): ITasksClient;
  public
    class var _resolver: TResolver;
    class function Create(const secret: TClientSecrets): ITasksClient; overload; static;
    class function Create(const clientId, clientSecret: string): ITasksClient; overload; static;
  end;


implementation

{ TasksClient }

class function TasksClient.Create(const secret: TClientSecrets): ITasksClient;
begin
  Assert(Assigned(_resolver), 'Tasks client resolver must be assigned');
  Result := _resolver(secret)
end;

class function TasksClient.Create(const clientId: string; const clientSecret: string): ITasksClient;
begin
  Result := TasksClient.Create(TClientSecrets.Create(clientId, clientSecret));  
end;


end.
