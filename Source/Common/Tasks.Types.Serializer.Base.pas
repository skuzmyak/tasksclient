unit Tasks.Types.Serializer.Base;

interface

uses
  Tasks.Types,
  Tasks.Types.Classes,
  Tasks.Types.Serializer;

type
  TResourceSerializerBase = class abstract(TResourceSerializer)
  protected
    const
      cDateTimeProps: array [0..2] of string = (
        TResource.TProperty.Due,
        TResource.TProperty.Completed,
        TResource.TProperty.Updated
      );
      cBoolProps: array [0..1] of string = (
        TResource.TProperty.Deleted,
        TResource.TProperty.Hidden
      );
  protected
    function ResolveResource(const value: IResource): TResource;
  public
    function DoSerialize(source: TResource): string; virtual; abstract;
    procedure DoPopulate(const source: string; target: TResource); virtual; abstract;

    function Serialize(const source: IResource): string; override;
    procedure Populate(const source: string; const target: IResource); override;
  end;


implementation

uses
  System.SysUtils;

{ TResourceSerializerBase }

procedure TResourceSerializerBase.Populate(const source: string; const target:
  IResource);
var
  src: TResource;
begin
  src := ResolveResource(target);
  DoPopulate(source, src);
end;

function TResourceSerializerBase.ResolveResource(const value: IResource): TResource;
var
  ref: IResourceReference;
begin
  if value.QueryInterface(IResourceReference, ref) = S_OK then
    Result := ref
  else //try alternative
    if (value as TObject) is TResource then
      Result := TResource(value)
    else
      Result := nil;

  if not Assigned(Result) then
    raise EArgumentException.Create('Unsupported implementation of resource');
end;

function TResourceSerializerBase.Serialize(const source: IResource): string;
var
  src: TResource;
begin
  src := ResolveResource(source);
  Result := DoSerialize(src);
end;

end.
