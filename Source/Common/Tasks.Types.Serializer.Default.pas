unit Tasks.Types.Serializer.Default;

interface

uses
  Tasks.Types,
//  Tasks.Types.
  Tasks.Types.Serializer;

type
  TResourceSerializerDefault = class(TResourceSerializer)

  public
    function Serialize(const source: IResource): string; override;
    procedure Populate(const source: string; const target: IResource); override;
  end;

implementation

uses
  Tasks.Types.Serializable,
  System.SysUtils;

{ TResourceSerializerDefault }

procedure TResourceSerializerDefault.Populate(const source: string; const target: IResource);
var
  dest: ISelfSerializable;
begin
  if target.QueryInterface(ISelfSerializable, dest) <> S_OK then
    raise EArgumentException.Create('Source does not support ISelfSerializable');

  dest.AsJson := source;
end;

function TResourceSerializerDefault.Serialize(const source: IResource): string;
var
  src: ISelfSerializable;
begin
  if source.QueryInterface(ISelfSerializable, src) <> S_OK then
    raise EArgumentException.Create('Source does not support ISelfSerializable');

  Result := src.ToJson();
end;

end.
