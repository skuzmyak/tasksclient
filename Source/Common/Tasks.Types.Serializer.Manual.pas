unit Tasks.Types.Serializer.Manual;

interface

uses
  Tasks.Types,
  Tasks.Types.Classes,
  Tasks.Types.Serializer.Base,

  JsonDataObjects;

type

  TResourceSerializerManual = class(TResourceSerializerBase)
  private type
    TProperty = Tasks.Types.Classes.TResource.TProperty;
  private
    function GetJsonDate(const value: TDateTime): string;
    function SerializeToJsonObject(source: TResource): TJsonObject;
    procedure PopulateFromJsonObject(source: TJsonObject; target: TResource);
  public
    procedure DoPopulate(const source: string; target: TResource); override;
    function DoSerialize(source: TResource): string; override;
  end;

implementation

uses
  Tasks.Types.Attributes,
  Tasks.Types.Helpers,
  Tasks.Types.Classes.Helpers,

  System.Rtti,
  System.Generics.Collections,
  System.StrUtils,

  Spring,
  Spring.Collections;

function TResourceSerializerManual.SerializeToJsonObject(source: TResource):
  TJsonObject;
var
  entity: TResourceEntity absolute source;
  task: TGoogleTask absolute source;
  tasks: TGoogleTaskCollection absolute source;
  taskLists: TGoogleTaskListCollection absolute source;

  json: TJsonObject;
  jsonArray: TJsonArray;
  item: TResourceEntity;
  itemList: TObjectList<TResourceEntity>;
begin
  itemList := nil;

  json := TJsonObject.Create;
  try
    json.S[TProperty.ETag] := source.ETag;
    json.S[TProperty.Kind] := source.Kind.AsString;
    if source is TResourceEntity then
    begin
      json.S[TProperty.Identifier] := entity.Id;
      json.S[TProperty.Updated] := GetJsonDate(entity.LastUpdatedUtc);
      json.S[TProperty.SelfLink] := entity.SelfLink;
      json.S[TProperty.Title] := entity.Title;

      if source is TGoogleTask then
      begin
        json.S[TProperty.Completed] := GetJsonDate(task.CompletedUtc);
        json.B[TProperty.Deleted] := task.Deleted;
        json.S[TProperty.Due] := GetJsonDate(task.DueUtc);
        json.B[TProperty.Hidden] := task.Hidden;
        json.S[TProperty.Notes] := task.Notes;
        json.S[TProperty.ParentTask] := task.ParentTaskId;
        json.S[TProperty.Position] := task.Position;
        json.S[TProperty.Status] := task.Status.AsString;
      end;
    end
    else
    if source is TGoogleTaskCollection then
      itemList := TObjectList<TResourceEntity>(tasks.Items)
    else
    if source is TGoogleTaskListCollection then
      itemList := TObjectList<TResourceEntity>(taskLists.Items)
    else
      raise EInvalidOperationException.Create('Unknown resource type');

    if itemList <> nil then
    begin    
      jsonArray := json.A[TProperty.Items];
      for item in itemList do
        jsonArray.Add(SerializeToJsonObject(item));
    end;      
  except
  {$IFNDEF AUTOREFCOUNT}
    json.Free;
  {$ELSE}
    json.DisposeOf;
  {$ENDIF}
    raise;
  end;

  Result := json;
end;

procedure TResourceSerializerManual.DoPopulate(const source: string; target: TResource);
var
  json: TJsonBaseObject;
  jsonObj: TJsonObject absolute json;
begin
  json := TJsonObject.Parse(source);
  try
    if not Assigned(json) or not (json is TJsonObject) then
      Guard.RaiseArgumentException('Source string does not contain valid Json object');

    PopulateFromJsonObject(jsonObj, target);
  finally
  {$IFNDEF AUTOREFCOUNT}
    json.Free;
  {$ELSE}
    json.DisposeOf;
  {$ENDIF}
  end;
end;

function TResourceSerializerManual.DoSerialize(source: TResource): string;
var
  jsonObj: TJsonObject;
begin
  jsonObj := SerializeToJsonObject(source);
  try
    jsonObj.TrimExcess;
    Result := jsonObj.ToJSON(True);
  finally
  {$IFNDEF AUTOREFCOUNT}
    jsonObj.Free;
  {$ELSE}
    jsonObj.DisposeOf;
  {$ENDIF}
  end;
end;

function TResourceSerializerManual.GetJsonDate(const value: TDateTime): string;
begin
  if value = NullDate then
    Result := ''
  else
    Result := TJsonObject.UtcDateTimeToJSON(value);
end;

procedure TResourceSerializerManual.PopulateFromJsonObject(source: TJsonObject;
  target: TResource);
var
  field: TRttiField;
  fields: IEnumerable<TRttiField>;
  jsonArray: TJsonArray;
  value: TValue;
  i: Integer;
  item: TResourceEntity;
  itemClass: TResourceClass;
  itemList: TObjectList<TResourceEntity>;
  propName: string;
  valueDateTime: TDateTime;
begin
  if (source.S[TResource.TProperty.Kind] = '')
    or (TResourceDataKind.FromString(source.S[TResource.TProperty.Kind]) <> target.Kind) then
    Guard.RaiseArgumentException('source json object is not valid');

  fields := target.GetSerializableFields;
  for field in fields do
  begin
    propName := target.SelectJsonPropertyName(field);
    value := TValue.Empty;

    if MatchText(propName, cDateTimeProps) then
    begin
      if source.IsNull(propName) then
        valueDateTime := NullDate
      else
        valueDateTime := source.DUtc[propName];
         
      value := TValue.From<TDateTime>(valueDateTime);
    end
    else
    if MatchText(propName, cBoolProps) then
      value := source.B[propName]
    else
    if propName = TResource.TProperty.Status then
      value := TValue.From<TGoogleTaskStatus>(TGoogleTaskStatus.FromString(source.S[propName]))
    else
    if propName = TResource.TProperty.Kind then
      value := TValue.Empty //no need to set
    else
    if propName = TResource.TProperty.Links then
      value := TValue.Empty //TODO
    else
    if propName = TResource.TProperty.Items then
    begin
      case target.Kind of
        TaskCollection: 
          begin        
            itemList := TObjectList<TResourceEntity>((target as TGoogleTaskCollection).Items);
            itemClass := TGoogleTask;
          end;          
        TaskListCollection: 
          begin        
            itemList := TObjectList<TResourceEntity>((target as TGoogleTaskListCollection).Items);
            itemClass := TGoogleTaskList; 
          end;          
      else
        raise EInvalidOperationException.Create('Unknown resource collection');
      end;

      jsonArray := source.A[propName];
      for i := 0 to jsonArray.Count - 1 do
      begin
        item := nil;
        try
          item := itemClass.Create() as TResourceEntity;
          PopulateFromJsonObject(jsonArray.Items[i]^.ObjectValue, item);
        except
        {$IFNDEF AUTOREFCOUNT}
          item.Free;
        {$ELSE}
          item.DisposeOf;
        {$ENDIF}
          raise;
        end;

        if item <> nil then
          itemList.Add(item);
      end;
    end
    else
      value := source.S[propName];

    if not value.IsEmpty then
      field.SetValue(target, value);
  end;
end;

end.
