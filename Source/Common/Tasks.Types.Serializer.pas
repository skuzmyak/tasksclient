unit Tasks.Types.Serializer;

interface

uses
  Tasks.Types;

type
  TResourceSerializerClass = class of TResourceSerializer;
  TResourceSerializer = class abstract
  public
    constructor Create; virtual;
    function Serialize(const source: IResource): string; overload; virtual;
    procedure Populate(const source: string; const target: IResource); virtual;

    function Serialize<I: IResource>(const source: I): string; overload;
    function Deserialize<I: IResource>(const source: string): I;
    function TryDeserialize<I: IResource>(const source: string; out target: I): Boolean;
  public
    const DefaultKey = '@default_serializer';
    class procedure RegisterSerializer(const key: string; clazz: TResourceSerializerClass);
    class procedure UnregisterSerializer(const key: string);
    class function CreateSerializer(const key: string): TResourceSerializer;

    class function CreateDefault: TResourceSerializer; static;
  end;


implementation

uses
  System.SysUtils,
  System.Generics.Collections;

  { TResourceSerializer }

constructor TResourceSerializer.Create;
begin
  inherited Create;
end;

class function TResourceSerializer.CreateDefault: TResourceSerializer;
begin
  Result := CreateSerializer(DefaultKey);
end;

function TResourceSerializer.Deserialize<I>(const source: string): I;
begin
  Result := TTasks.Create<I>;
  Self.Populate(source, Result);
end;

procedure TResourceSerializer.Populate(const source: string; const target:
  IResource);
begin
  raise ENotImplemented.Create('Unable to populate resource');
end;

function TResourceSerializer.Serialize(const source: IResource): string;
begin
  raise ENotImplemented.Create('Unable to serialize resource');
end;

function TResourceSerializer.Serialize<I>(const source: I): string;
begin
  Result := Self.Serialize(source);
end;

function TResourceSerializer.TryDeserialize<I>(const source: string; out target: I): Boolean;
begin
  try
    target := Self.Deserialize<I>(source);
    Result := Assigned(target);
  except
    Result := False;
    target := nil;
  end;
end;

var
  _serializers: TDictionary<string, TResourceSerializerClass>;

class procedure TResourceSerializer.RegisterSerializer(const key: string; clazz: TResourceSerializerClass);
begin
  if _serializers.ContainsKey(key) then
    raise EArgumentException.Create('serializer with such key already registered');
  _serializers.Add(key, clazz);
end;

class procedure TResourceSerializer.UnregisterSerializer(const key: string);
begin
  _serializers.Remove(key);
end;

class function TResourceSerializer.CreateSerializer(const key: string): TResourceSerializer;
var
  clazz: TResourceSerializerClass;
begin
  if not _serializers.TryGetValue(key, clazz) then
    raise EArgumentException.Create('No such key registered');

  Result := clazz.Create;
end;

initialization
  _serializers := TDictionary<string, TResourceSerializerClass>.Create();

finalization
  FreeAndNil(_serializers);

end.
