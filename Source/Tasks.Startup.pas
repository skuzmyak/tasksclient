unit Tasks.Startup;

interface


implementation

uses
  Tasks.Types,
  Tasks.Types.Registration,

  Tasks.Client,
  Tasks.Client.Default,
  Tasks.Client.Registration,

  Tasks.Types.Serializer,
  Tasks.Types.Serializer.Default
  ;


initialization
  TTasksTypesRegistrator.BindDefaultResolver;

  TTasksClientRegistrator.BindDefaultResolver;

  TResourceSerializer.RegisterSerializer(TResourceSerializer.DefaultKey, TResourceSerializerDefault);


end.
