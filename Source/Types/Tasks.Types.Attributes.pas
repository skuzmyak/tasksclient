unit Tasks.Types.Attributes;

interface

type
  ObjectPropertyAttribute = class(TCustomAttribute)
  private
    fName: string;
  public
    constructor Create(const value: string);
    property Name: string read fName;
  end;

  JsonPropertyAttribute = class(ObjectPropertyAttribute);

  //alias
  PropName = JsonPropertyAttribute;

implementation


{ ObjectPropertyAttribute }

constructor ObjectPropertyAttribute.Create(const value: string);
begin
  fName := value;
end;


end.
