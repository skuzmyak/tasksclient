unit Tasks.Types.Classes.Helpers;

interface

uses
  Tasks.Types.Classes,
  System.Rtti,
  Spring,
  Spring.Collections;


type

  TResourceHelper = class helper for TResource
  private
    class var fLock: Lock;
    class var fSrializableFields: IMultiMap<TClass, TRttiField>;
  public
    function GetSerializableFieldNames: IEnumerable<string>;
    function GetSerializableFields: IEnumerable<TRttiField>;
    function SelectFieldName(Value: TRttiField): string;
    function SelectJsonPropertyName(Value: TRttiField): string;
  end;


implementation

uses
  Tasks.Types.Attributes,
  Spring.Reflection;

{ TResourceHelper }

function TResourceHelper.GetSerializableFieldNames: IEnumerable<string>;
begin
  Result := TEnumerable.Select<TRttiField, string>(GetSerializableFields, SelectFieldName);
end;

function TResourceHelper.GetSerializableFields: IEnumerable<TRttiField>;
var
  fields: TArray<TRttiField>;
  res: IReadOnlyList<TRttiField>;
  field: TRttiField;
  resourceProp: JsonPropertyAttribute;
begin
  if not Assigned(fSrializableFields) then
  begin
    fLock.Enter;
    try
      if not Assigned(fSrializableFields) then
        fSrializableFields := TCollections.CreateMultiMap<TClass, TRttiField>;
    finally
      fLock.Leave;
    end;
  end;

  if not fSrializableFields.TryGetValues(Self.ClassType, res) then
  begin
    fLock.Enter;
    try
      fields := TType.GetType(Self.ClassType).GetFields;
      for field in fields do
      begin
        if field.TryGetCustomAttribute<JsonPropertyAttribute>(resourceProp) then
          fSrializableFields.Add(Self.ClassType, field);
      end;
      res := fSrializableFields.Items[Self.ClassType];
    finally
      fLock.Leave;
    end;
  end;

  Result := res;
end;

function TResourceHelper.SelectFieldName(Value: TRttiField): string;
begin
  Result := Value.Name;
end;

function TResourceHelper.SelectJsonPropertyName(Value: TRttiField): string;
begin
  Result := Value.GetCustomAttribute<JsonPropertyAttribute>().Name;
end;


end.
