unit Tasks.Types.Classes.Serializable;

interface

uses
  Tasks.Types.Classes,
  Tasks.Types.Serializable,
  JsonDataObjects;

type
  TResource = class(Tasks.Types.Classes.TResource)
  strict protected
    procedure Read(const data: TJsonObject); virtual;
  end;

  TGoogleTask = class(Tasks.Types.Classes.TGoogleTask)
  strict protected
    procedure Read(const data: TJsonObject); override;
  end;

implementation

{ TResource }

procedure TResource.Read(const data: TJsonObject);
begin

end;

{ TGoogleTask }

procedure TGoogleTask.Read(const data: TJsonObject);
begin
  inherited;

end;

end.
