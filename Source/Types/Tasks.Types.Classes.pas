﻿unit Tasks.Types.Classes;

interface

uses
  Tasks.Types,
  Tasks.Types.Attributes,

  System.Generics.Collections,
  Spring,
  JsonDataObjects;

type
  TResource = class abstract(TAggregatedObject)
  public type
    TProperty = class
    public const
      Kind = 'kind';
      ETag = 'etag';
      NextPageToken = 'nextPageToken';
      Items = 'items';
      Identifier = 'id';
      Title = 'title';
      Updated = 'updated';
      SelfLink = 'selfLink';
      Position = 'position';
      Notes = 'notes';
      Status = 'status';
      ParentTask = 'parent';
      Due = 'due';
      Completed = 'completed';
      Deleted = 'deleted';
      Hidden = 'hidden';
      Links = 'links';
      LinksType = 'type';
      LinksDesc = 'description';
      LinksLink = 'link';
     end;
  strict private
    [PropName(TProperty.ETag)]
    fETag: string;
    [PropName(TProperty.Kind)]  //for serialization
    fKind: TResourceDataKind;
  strict protected
    function GetETag: string;
    function GetKind: TResourceDataKind; virtual;
  public
    constructor Create; virtual;
    constructor CreateAggregated(const owner: IInterface);

    procedure Read(value: TJsonObject); virtual;
    procedure Write(value: TJsonObject); virtual;

    property ETag: string read GetETag;
    property Kind: TResourceDataKind read GetKind;
  end;

  TResourceEntity = class abstract(TResource, IResourceEntity)
  strict private
    [PropName(TResource.TProperty.Identifier)]
    fId: string;
    [PropName(TResource.TProperty.Updated)]
    fLastUpdatedUtc: TDateTime;
    [PropName(TResource.TProperty.SelfLink)]
    fSelfLink: string;
    [PropName(TResource.TProperty.Title)]
    fTitle: string;
  strict protected
    function GetId: string;
    function GetLastUpdatedUtc: TDateTime;
    function GetSelfLink: string;
    function GetTitle: string;
    procedure SetTitle(const Value: string);
  public
    constructor Create; override;

    procedure Read(value: TJsonObject); override;
    procedure Write(value: TJsonObject); override;

    property Id: string read GetId;
    /// <summary>
    ///   Last modification time of the task (as a RFC 3339 timestamp).
    /// </summary>
    property LastUpdatedUtc: TDateTime read GetLastUpdatedUtc;
    /// <summary>
    ///   URL pointing to this task. Used to retrieve, update, or delete this task
    /// </summary>
    property SelfLink: string read GetSelfLink;
    property Title: string read GetTitle write SetTitle;
  end;

  TGoogleTask = class(TResourceEntity)
  strict private
    [PropName(TResource.TProperty.Completed)]
    fCompletedUtc: TDateTime;
    [PropName(TResource.TProperty.Deleted)]
    fDeleted: Boolean;
    [PropName(TResource.TProperty.Due)]
    fDueUtc: TDateTime;
    [PropName(TResource.TProperty.Hidden)]
    fHidden: Boolean;
    [PropName(TResource.TProperty.Links)]
    fLinks: TList<TTaskLink>;
    [PropName(TResource.TProperty.Notes)]
    fNotes: string;
    [PropName(TResource.TProperty.ParentTask)]
    fParentTaskId: string;
    [PropName(TResource.TProperty.Position)]
    fPosition: string;
    [PropName(TResource.TProperty.Status)]
    fStatus: TGoogleTaskStatus;

    fOnSatusChanged: Event<TTaskStatusChangedEvent>;
  strict protected
    function GetCompletedUtc: TDateTime;
    function GetDeleted: Boolean;
    function GetDueUtc: TDateTime;
    function GetHasDueDate: Boolean;
    function GetHidden: Boolean;
    function GetKind: TResourceDataKind; override;
    function GetLinks: TArray<TTaskLink>;
    function GetNotes: string;
    function GetOnStatusChange: IEvent<TTaskStatusChangedEvent>;
    function GetParentTaskId: string;
    function GetPosition: string;
    function GetStatus: TGoogleTaskStatus;
    procedure SetDueUtc(const Value: TDateTime);
    procedure SetNotes(const Value: string);
    procedure SetStatus(const Value: TGoogleTaskStatus);
  public
    constructor Create; override;
    destructor Destroy; override;

    procedure Read(value: TJsonObject); override;
    procedure Write(value: TJsonObject); override;
    /// <summary>IGoogleTask.CompletedUtc
    /// Completion date of the task (as a RFC 3339 timestamp). This field is omitted if
    /// the task has not been completed.
    /// </summary>
    /// type:TDateTime
    property CompletedUtc: TDateTime read GetCompletedUtc;
    /// <summary>IGoogleTask.Deleted
    /// Flag indicating whether the task has been deleted. The default if False.
    /// </summary>
    /// type:Boolean
    property Deleted: Boolean read GetDeleted;
    /// <summary>IGoogleTask.DueUtc
    /// Due date of the task (as a RFC 3339 timestamp). Optional.
    /// </summary>
    /// type:TDateTime
    property DueUtc: TDateTime read GetDueUtc write SetDueUtc;
    property HasDueDate: Boolean read GetHasDueDate;
    /// <summary>IGoogleTask.Hidden
    /// Flag indicating whether the task is hidden. This is the case if the task had
    /// been marked completed when the task list was last cleared. The default is False.
    /// This field is read-only.
    /// </summary>
    /// type:Boolean
    property Hidden: Boolean read GetHidden;
    /// <summary>IGoogleTask.Links
    /// Collection of links. This collection is read-only
    /// </summary> type:TArray<TTaskLink>
    property Links: TArray<TTaskLink> read GetLinks;
    /// <summary>
    ///   Notes describing the task. Optional
    /// </summary>
    property Notes: string read GetNotes write SetNotes;
    /// <summary>
    ///   Parent task identifier.
    ///This field is omitted if it is a top-level task. This field is read-only.
    ///Use the "move" method to move the task under a different parent or to the top level.
    /// </summary>
    property ParentTaskId: string read GetParentTaskId;
    /// <summary>
    ///   String indicating the position of the task among its sibling tasks under the same parent task
    ///or at the top level. If this string is greater than another task's corresponding position string
    /// according to lexicographical ordering, the task is positioned after the other task under
    ///the same parent task (or at the top level).
    ///This field is read-only. Use the "move" method to move the task to another position.
    /// </summary>
    property Position: string read GetPosition;
    /// <summary>IGoogleTask.Status
    /// Status of the task. This is either "needsAction" or "completed".
    /// </summary>
    /// type:TTaskStatus
    property Status: TGoogleTaskStatus read GetStatus write SetStatus;
    property OnStatusChanged: IEvent<TTaskStatusChangedEvent> read GetOnStatusChange;
  end;

  TGoogleTaskList = class(TResourceEntity)
  strict protected
    function GetKind: TResourceDataKind; override;
  end;

  TCollectionNotification = System.Generics.Collections.TCollectionNotification;

  TResourceCollection<T: TResourceEntity> = class abstract(TResource)
  strict private
    [PropName(TResource.TProperty.Items)]
    fItems: TObjectList<T>;
  public
    destructor Destroy; override;
    procedure AfterConstruction; override;

    procedure Read(value: TJsonObject); override;
    procedure Write(value: TJsonObject); override;

    property Items: TObjectList<T> read fItems;
  end;

  TGoogleTaskCollection = class(TResourceCollection<TGoogleTask>)
  strict protected
    function GetKind: TResourceDataKind; override;
  end;

  TGoogleTaskListCollection = class(TResourceCollection<TGoogleTaskList>)
  strict protected
    function GetKind: TResourceDataKind; override;
  end;

  /// <summary>
  ///  Returns underling object (for serialization). Must have valid GUID.
  /// </summary>
  IResourceReference = interface(IShared<TResource>)
  ['{A46DBEFF-E8D1-44BB-9FC5-D8EA321B89E8}']
  end;

  TResourceClass = class of TResource;

const NullDate = 0;

implementation

uses
  Tasks.Types.Helpers,
  System.SysUtils;


type 
  TTaskLinkHelper = class helper for TTaskLink
  public
    procedure Read(value: TJsonObject);
    procedure Write(value: TJsonObject);
  end;

constructor TResource.CreateAggregated(const owner: IInterface);
begin
  Guard.CheckNotNull(owner, 'owner');
  inherited Create(owner);
  Create;//virtual contructor call
end;

constructor TResource.Create;
begin
  fKind := Self.GetKind;
end;

function TResource.GetETag: string;
begin
  Result := fETag;
end;

function TResource.GetKind: TResourceDataKind;
begin
  raise ENotImplemented.Create('Unknown resource data kind');
end;

procedure TResource.Read(value: TJsonObject);
var
  kind: TResourceDataKind;
begin
  //it will fail if empty
  kind.AsString := value.S[TProperty.Kind];
  if kind <> Self.Kind then
    raise EArgumentException.Create('Invalid json content');

  fETag := value.S[TProperty.ETag];
end;

procedure TResource.Write(value: TJsonObject);
begin
  value.S[TProperty.ETag] := fETag;
  value.S[TProperty.Kind] := Self.Kind.AsString;
end;

{TResourceEntity}

constructor TResourceEntity.Create;
begin
  inherited;
  fLastUpdatedUtc := NullDate;
end;

function TResourceEntity.GetId: string;
begin
  Result := fId;
end;

function TResourceEntity.GetLastUpdatedUtc: TDateTime;
begin
  Result := fLastUpdatedUtc;
end;

function TResourceEntity.GetSelfLink: string;
begin
  Result := fSelfLink;
end;

function TResourceEntity.GetTitle: string;
begin
  Result := fTitle;
end;

procedure TResourceEntity.Read(value: TJsonObject);
begin
  inherited;
  fId := value.S[TProperty.Identifier];
  fLastUpdatedUtc := value.DUtc[TProperty.Updated];
  fSelfLink := value.S[TProperty.SelfLink];
  fTitle := value.S[TProperty.Title];
end;

procedure TResourceEntity.Write(value: TJsonObject);
begin
  inherited;
  value.S[TProperty.Identifier] := fId;
  if fLastUpdatedUtc <> NullDate then
    value.DUtc[TProperty.Updated] := fLastUpdatedUtc;
  value.S[TProperty.SelfLink] := fSelfLink;
  value.S[TProperty.Title] := fTitle;
end;

procedure TResourceEntity.SetTitle(const Value: string);
begin
  fTitle := Value;
end;


{TGoogleTask}

constructor TGoogleTask.Create;
begin
  inherited Create;
  fLinks := TObjectList<TTaskLink>.Create(True);
  fDueUtc := NullDate;
  fCompletedUtc := NullDate;
  fStatus := TGoogleTaskStatus.NeedsAction;
end;

destructor TGoogleTask.Destroy;
begin
  FreeAndNil(fLinks);
  inherited;
end;

function TGoogleTask.GetCompletedUtc: TDateTime;
begin
  Result := fCompletedUtc;
end;

function TGoogleTask.GetDeleted: Boolean;
begin
  Result := fDeleted;
end;

function TGoogleTask.GetDueUtc: TDateTime;
begin
  Result := fDueUtc;
end;

function TGoogleTask.GetHasDueDate: Boolean;
begin
  Result := fDueUtc <> NullDate;
end;

function TGoogleTask.GetHidden: Boolean;
begin
  Result := fHidden;
end;

function TGoogleTask.GetKind: TResourceDataKind;
begin
  Result := TResourceDataKind.Task;
end;

function TGoogleTask.GetLinks: TArray<TTaskLink>;
begin
  Result := fLinks.ToArray;
end;

function TGoogleTask.GetNotes: string;
begin
  Result := fNotes;
end;

function TGoogleTask.GetOnStatusChange: IEvent<TTaskStatusChangedEvent>;
begin
  Result := fOnSatusChanged;
end;

function TGoogleTask.GetParentTaskId: string;
begin
  Result := fParentTaskId;
end;

function TGoogleTask.GetPosition: string;
begin
  Result := fPosition;
end;

function TGoogleTask.GetStatus: TGoogleTaskStatus;
begin
  Result := fStatus;
end;

procedure TGoogleTask.Read(value: TJsonObject);
var  
  links: TJsonArray;
  linkItem: TJsonObject;
  link: TTaskLink;
begin
  inherited;
  fCompletedUtc := value.DUtc[TProperty.Completed];
  fDeleted := value.B[TProperty.Deleted];
  fDueUtc := value.DUtc[TProperty.Due];
  fHidden := value.B[TProperty.Hidden];
  fNotes := value.S[TProperty.Notes];
  fParentTaskId := value.S[TProperty.ParentTask];
  fPosition := value.S[TProperty.Position];
  fStatus.AsString := value.S[TProperty.Status]; 
  
  fLinks.Clear;
  links := value.A[TProperty.Links];
  for linkItem in links do
  begin
    link := TTaskLink.Create;
    link.Read(linkItem);
    fLinks.Add(link); 
  end;  
end;

procedure TGoogleTask.Write(value: TJsonObject);
var  
  jsonArray: TJsonArray;
  jsonItem: TJsonObject;
  link: TTaskLink;
begin
  inherited;
  if fCompletedUtc <> NullDate then
    value.DUtc[TProperty.Completed] := fCompletedUtc;
  value.B[TProperty.Deleted] := fDeleted;
  if fDueUtc <> NullDate then
    value.DUtc[TProperty.Due] := fDueUtc;
  value.B[TProperty.Hidden] := fHidden;
  value.S[TProperty.Notes] := fNotes;
  value.S[TProperty.ParentTask] := fParentTaskId;
  value.S[TProperty.Position] := fPosition;
  value.S[TProperty.Status] := fStatus.AsString;

  jsonArray := value.A[TProperty.Links];
  for link in fLinks do
  begin
    jsonItem := TJsonObject.Create;
    link.Write(jsonItem);
    jsonArray.Add(jsonItem);
  end;  
end;

procedure TGoogleTask.SetDueUtc(const Value: TDateTime);
begin
  fDueUtc := Value;
end;

procedure TGoogleTask.SetNotes(const Value: string);
begin
  fNotes := Value;
end;

procedure TGoogleTask.SetStatus(const Value: TGoogleTaskStatus);
begin
  if fStatus <> Value then
  begin
    fStatus := Value;
    fOnSatusChanged.Invoke(Self, fStatus);
  end;
end;

procedure TResourceCollection<T>.AfterConstruction;
begin
  inherited;
  //when object created aggregated - items are NOT owned
  fItems := TObjectList<T>.Create(not Assigned(Self.Controller));
end;

destructor TResourceCollection<T>.Destroy;
begin
  FreeAndNil(fItems);
  inherited;
end;

procedure TResourceCollection<T>.Read(value: TJsonObject);
var
  jsonArray: TJsonArray;
  jsonItem: TJsonObject;
  item: T;
begin
  inherited;
  fItems.Clear;
  jsonArray := value.A[TProperty.Items];
  for jsonItem in jsonArray do
  begin
    item := T.Create;
    try
      item.Read(jsonItem);
    except
      item.Free;
      raise;
    end;
    fItems.Add(item);
  end;  
end;

procedure TResourceCollection<T>.Write(value: TJsonObject);
var
  jsonArray: TJsonArray;
  jsonItem: TJsonObject;
  item: T;
begin
  inherited;
  jsonArray := value.A[TProperty.Items];
  for item in fItems do
  begin
    jsonItem := TJsonObject.Create;
    try
      item.Write(jsonItem);
    except
      jsonItem.Free;
      raise;
    end;
    jsonArray.Add(jsonItem);
  end;

end;

{ TGoogleTaskList }

function TGoogleTaskList.GetKind: TResourceDataKind;
begin
  Result := TResourceDataKind.TaskList;
end;

{ TGoogleTaskCollection }

function TGoogleTaskCollection.GetKind: TResourceDataKind;
begin
  Result := TResourceDataKind.TaskCollection;
end;

{ TGoogleTaskListCollection }

function TGoogleTaskListCollection.GetKind: TResourceDataKind;
begin
  Result := TResourceDataKind.TaskListCollection; 
end;


{ TTaskLinkHelper }

procedure TTaskLinkHelper.Read(value: TJsonObject);
begin
  &Type := value.S[TResource.TProperty.LinksType];
  Description := value.S[TResource.TProperty.LinksDesc];
  Link := value.S[TResource.TProperty.LinksLink];
end;

procedure TTaskLinkHelper.Write(value: TJsonObject);
begin
  value.S[TResource.TProperty.LinksType] := &Type;
  value.S[TResource.TProperty.LinksDesc] := Description;
  value.S[TResource.TProperty.LinksLink] := Link;
end;

end.
