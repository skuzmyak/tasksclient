unit Tasks.Types.Default;

interface

uses
  Tasks.Types,
  Tasks.Types.Serializable,
  Tasks.Types.Classes,

  Spring,
  Spring.Collections,
  JsonDataObjects;

type
  TInterfacedResource = class abstract(TInterfacedObject)
  protected type
    TProperty = TResource.TProperty;
  public
    constructor Create; virtual;
  end;

  TInterfacedResourceClass = class of TInterfacedResource;

  ISerializable = interface
  ['{F2EE459A-60A1-4F69-AB8D-A4DC19E251F5}']
    procedure Read(value: TJsonObject);
    procedure Write(value: TJsonObject);
  end;
                
  TInterfacedResourceBase<T: TResource> = class abstract(
    TInterfacedResource,
    IResource,
    ISerializable,
    ISelfSerializable
  )
  strict private
    function GetETag: string;
    function GetKind: TResourceDataKind;

    function GetAsJson: string;
    procedure SetAsJson(const Value: string);
    function ToJson(trimExcess: Boolean = True; compact: Boolean = True): string;
  strict protected
    fResource: T;
    function GetResourceClass: TResourceClass; virtual;

    procedure Read(value: TJsonObject); virtual;
    procedure Write(value: TJsonObject);  virtual;
  public
    constructor Create; override;
    destructor Destroy; override;

    property Resource: T read fResource;
  end;

  TReferencedInterfacedResource<T: TResource> = class(TInterfacedResourceBase<T>, IResourceReference)
  strict private
    function Invoke: TResource;
  public
    constructor CreateFrom(const value: T);
  end;

  TInterfacedGoogleTask = class(TReferencedInterfacedResource<TGoogleTask>, IGoogleTask, IResourceEntity)
  public
    property Task: TGoogleTask read fResource implements IGoogleTask, IResourceEntity;
  end;

  TInterfacedGoogleTaskList = class(TReferencedInterfacedResource<TGoogleTaskList>, IGoogleTaskList, IResourceEntity)
  public
    property TaskList: TGoogleTaskList read fResource implements IGoogleTaskList, IResourceEntity;
  end;

  TInterfacedResourceCollection<T: TResourceEntity; I: IResourceEntity> = class abstract(
    TReferencedInterfacedResource<TResourceCollection<T>>, IResourceCollection<I>)
  strict private
    fCollection: IList<I>;
    function GetItems: IEnumerable<I>;
    procedure ResourceItemsChangedEvent(Sender: TObject; const item: T;
      action: TCollectionNotification);
  public
    procedure AfterConstruction; override;
  end;

  TInterfacedGoogleTaskCollection = class(TInterfacedResourceCollection<TGoogleTask, IGoogleTask>,
    IGoogleTaskCollection)
  strict protected
    function GetResourceClass: TResourceClass; override;
  end;

  TInterfacedGoogleTaskListCollection = class(TInterfacedResourceCollection<TGoogleTaskList, IGoogleTaskList>,
    IGoogleTaskListCollection)
  strict protected
    function GetResourceClass: TResourceClass; override;
  end;

  TResourceCollectionItemFactory = class(TResource)
  private const
  public
    function CreateItem<I: IResourceEntity>: I;
  end;

  TTaskItemFactory = class(TResourceCollectionItemFactory)
  strict protected
    function GetKind: TResourceDataKind; override;
  end;

  TTaskListItemFactory = class(TResourceCollectionItemFactory)
  strict protected
    function GetKind: TResourceDataKind; override;
  end;

  TInterfacedResourceCollectionSimple<T: TResourceCollectionItemFactory; I: IResourceEntity> =
    class(TInterfacedResourceBase<T>, IResourceCollection<I>)
  strict private
    fItems: IList<I>;
  strict protected
    function GetItems: IEnumerable<I>;
    procedure Read(value: TJsonObject); override;
    procedure Write(value: TJsonObject);  override;
  public
    constructor Create; override;
  end;

  TInterfacedGoogleTaskCollectionSimple = class(
    TInterfacedResourceCollectionSimple<TTaskItemFactory, IGoogleTask>,
    IGoogleTaskCollection
  );

  TInterfacedGoogleTaskListCollectionSimple = class(
    TInterfacedResourceCollectionSimple<TTaskListItemFactory, IGoogleTaskList>,
    IGoogleTaskListCollection
  );

const
  cCollectionKindToItemClass: array [TResourceDataKind] of TInterfacedResourceClass = (
    nil,
    nil,
    TInterfacedGoogleTask,
    TInterfacedGoogleTaskList
  );

implementation

uses
  System.SysUtils,
  Tasks.Types.Helpers;

{ TInterfacedResourceBase<T> }

constructor TInterfacedResourceBase<T>.Create;
begin
  inherited Create;
  fResource := GetResourceClass.CreateAggregated(Self) as T;
end;

destructor TInterfacedResourceBase<T>.Destroy;
begin
  FreeAndNil(fResource);
  inherited;
end;

function TInterfacedResourceBase<T>.GetAsJson: string;
begin
  Result := Self.ToJson();
end;

function TInterfacedResourceBase<T>.GetETag: string;
begin
  Result := fResource.ETag;
end;

function TInterfacedResourceBase<T>.GetKind: TResourceDataKind;
begin
  Result := fResource.Kind;
end;

function TInterfacedResourceBase<T>.GetResourceClass: TResourceClass;
begin
  Result := T;
end;

procedure TInterfacedResourceBase<T>.Read(value: TJsonObject);
begin
  Resource.Read(value);
end;

procedure TInterfacedResourceBase<T>.SetAsJson(const Value: string);
var
  json: Shared<TJsonObject>;
begin
  json := TJsonObject.Parse(Value) as TJsonObject;
  Self.Read(json.Value);
end;

function TInterfacedResourceBase<T>.ToJson(trimExcess, compact: Boolean): string;
var
  json: Shared<TJsonObject>;
begin
  json := TJsonObject.Create;
  Self.Write(json.Value);
  if trimExcess then
    json.Value.TrimExcess;
  Result := json.Value.ToJSON(compact);
end;

procedure TInterfacedResourceBase<T>.Write(value: TJsonObject);
begin
  Resource.Write(value);
end;

procedure TInterfacedResourceCollection<T, I>.AfterConstruction;
begin
  inherited;
  fCollection := TCollections.CreateInterfaceList<I>;
  fResource.Items.OwnsObjects := False;
  fResource.Items.OnNotify := ResourceItemsChangedEvent;
end;

function TInterfacedResourceCollection<T, I>.GetItems: IEnumerable<I>;
begin
  Result := fCollection;
end;

procedure TInterfacedResourceCollection<T, I>.ResourceItemsChangedEvent(Sender:
  TObject; const item: T; action: TCollectionNotification);
var
  intf: IResource;
  intfItem: I;
begin

  if action = TCollectionNotification.cnAdded then
  begin
    intf := TReferencedInterfacedResource<T>(cCollectionKindToItemClass[fResource.Kind]).CreateFrom(item);

//    case fResource.Kind of
//      TResourceDataKind.TaskCollection: infItem := TInterfacedGoogleTask.CreateFrom(item as TGoogleTask) as IGoogleTask;
//      TResourceDataKind.TaskListCollection: infItem := TInterfacedGoogleTaskList.CreateFrom(item as TGoogleTaskList) as IGoogleTaskList;
//    else
//      raise EInvalidOperationException.Create('Not supported resource data kind');
//    end;
    if not TTasks.TryAsType<I>(intf, intfItem) then
      Assert(False);

    fCollection.Add(intfItem)
  end
  else
    fCollection.RemoveAll(
      function (const value: I): Boolean
      begin
        Result := (value as TInterfacedResourceBase<T>).Resource  = item;
      end
    );
end;

function TInterfacedGoogleTaskCollection.GetResourceClass: TResourceClass;
begin
  //must return correct class with valid kind
  Result := TGoogleTaskCollection;
end;

function TInterfacedGoogleTaskListCollection.GetResourceClass: TResourceClass;
begin
  //must return correct class with valid kind
  Result := TGoogleTaskListCollection;
end;

{ TInterfacedResource }

constructor TInterfacedResource.Create;
begin
  inherited Create;  
end;

{ TReferencedInterfacedResource<T> }

constructor TReferencedInterfacedResource<T>.CreateFrom(const value: T);
begin
  Guard.CheckNotNull<T>(value, 'value');
  if Assigned(value.Controller) then
    raise EInvalidOpException.Create('Source item <T> must not have explicit controller');

  fResource := value;

end;

function TReferencedInterfacedResource<T>.Invoke: TResource;
begin
  Result := Self.Resource;
end;

{ TResourceCollectionItemFactory<I> }


function TResourceCollectionItemFactory.CreateItem<I>: I;
var
  item: IInterface;
begin
  item := cCollectionKindToItemClass[Self.Kind].Create;
  if not TTasks.TryAsType<I>(item, Result) then
    Assert(False);
end;

{ TResourceCollectionTaskListFactory }

function TTaskListItemFactory.GetKind: TResourceDataKind;
begin
  Result := TResourceDataKind.TaskListCollection;
end;

{ TResourceCollectionTaskFactory }

function TTaskItemFactory.GetKind: TResourceDataKind;
begin
  Result := TResourceDataKind.TaskCollection;
end;

{ TInterfacedResourceCollectionSimple<T, I> }

constructor TInterfacedResourceCollectionSimple<T, I>.Create;
begin
  inherited;
  fItems := TCollections.CreateList<I>();
end;

function TInterfacedResourceCollectionSimple<T, I>.GetItems: IEnumerable<I>;
begin
  Result := fItems;
end;

procedure TInterfacedResourceCollectionSimple<T, I>.Read(value: TJsonObject);
var
  jsonArray: TJsonArray;
  jsonItem: TJsonObject;
  item: I;
begin
  inherited;
  fItems.Clear;
  jsonArray := value.A[TProperty.Items];
  for jsonItem in jsonArray do
  begin
    item := Self.Resource.CreateItem<I>;
    (IResourceEntity(item) as ISerializable).Read(jsonItem);
    fItems.Add(item);
  end;
end;

procedure TInterfacedResourceCollectionSimple<T, I>.Write(value: TJsonObject);
var
  jsonArray: TJsonArray;
  jsonItem: TJsonObject;
  item: I;
begin
  inherited;
  jsonArray := value.A[TProperty.Items];
  for item in fItems do
  begin
    jsonItem := TJsonObject.Create;
    try
      (IResourceEntity(item) as ISerializable).Write(jsonItem);
      jsonArray.Add(jsonItem);
    except
      jsonItem.Free;
      raise;
    end;
  end;
end;


end.
