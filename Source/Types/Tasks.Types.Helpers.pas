unit Tasks.Types.Helpers;

interface

uses
  Tasks.Types,

  System.TypInfo,
  System.SysUtils,
  System.Generics.Defaults,
  JsonDataObjects;

type
  TEntityComparerById<T: IResourceEntity> = class(TCustomComparer<T>)
  private
    class var fSingleton: TEntityComparerById<T>;
  protected
    function Compare(const Left, Right: T): Integer; override;
    function Equals(const Left, Right: T): Boolean; override;
    function GetHashCode(const Value: T): Integer; override;
  public
    class destructor _destroy;
    class function GetSingleton: TEntityComparerById<T>; static;
    class function GetEqualityComparer: IEqualityComparer<T>; static;
    class function GetEqualityComparison: TEqualityComparison<T>; static;
    class function GetComparer: IComparer<T>; static;
  end;

  TResourceDataKindHelper = record helper for TResourceDataKind
  private const
    StrMap: array[TResourceDataKind] of string = (
      'tasks#task',
      'tasks#taskList',
      'tasks#tasks',
      'tasks#taskLists'
    );
  private
    function GetAsString: string;
    procedure SetAsString(const Value: string);
  public
    class function FromString(const value: string): TResourceDataKind; static;
    property AsString: string read GetAsString write SetAsString;
  end;

  TGoogleTaskStatusHelper = record helper for TGoogleTaskStatus
  private const
    StrMap: array[TGoogleTaskStatus] of string = ('needsAction', 'completed');
  private
    function GetAsString: string;
    procedure SetAsString(const Value: string);
  public
    class function FromString(const value: string): TGoogleTaskStatus; static;
    property AsString: string read GetAsString write SetAsString;
  end;

  TEnumHelper = class
    class function FromString<T>(const value: string; const mapper: TFunc<T, string>): T; static;
  end;

  TTaskComparerById = TEntityComparerById<IGoogleTask>;
  TTaskListComparerById = TEntityComparerById<IGoogleTaskList>;

  TTasksHelper = record helper for TTasks
  public
    class function TryAsType(const typInfo: PTypeInfo; const value: IInterface;
      out intf): Boolean; overload; static;
    class function TryAsType<I: IResource>(const value: IInterface;
      out intf: I): Boolean; overload; static;
  end;

  TJsonObject = JsonDataObjects.TJsonObject;
  TJsonArray = JsonDataObjects.TJsonArray;
  PJsonDataValue = JsonDataObjects.PJsonDataValue;

  TJsonObjectHelper = class helper for TJsonObject
  public
    procedure TrimExcess;
  end;

implementation


{TResourceDataKindHelper}

class function TResourceDataKindHelper.FromString(const value: string): TResourceDataKind;
begin
  Result := TEnumHelper.FromString<TResourceDataKind>(
    value,
    function(val: TResourceDataKind): string
    begin
      Result := val.AsString;
    end
  );
end;

function TResourceDataKindHelper.GetAsString: string;
begin
  Result := StrMap[Self];
end;

procedure TResourceDataKindHelper.SetAsString(const Value: string);
begin
  Self := FromString(Value);
end;

{TGoogleTaskStatusHelper}

class function TGoogleTaskStatusHelper.FromString(const value: string): TGoogleTaskStatus;
begin
  Result := TEnumHelper.FromString<TGoogleTaskStatus>(
    value,
    function(val: TGoogleTaskStatus): string
    begin
      Result := val.AsString;
    end
  );
end;

function TGoogleTaskStatusHelper.GetAsString: string;
begin
  Result := StrMap[Self];
end;

procedure TGoogleTaskStatusHelper.SetAsString(const Value: string);
begin
  Self := FromString(Value);
end;

{ TEnumHelper }

class function TEnumHelper.FromString<T>(const value: string; const mapper: TFunc<T, string>): T;
var
  data: PTypeData;
  i: Integer;
  val: T;
begin
  data := GetTypeData(TypeInfo(T));
  for i := data^.MinValue to data^.MaxValue do
  begin
    Move(i, val, SizeOf(T));
    if CompareText(mapper(val), value) = 0 then
      Exit(val);
  end;

  raise EArgumentException.Create('Invalid string representation of enum');
end;

function TEntityComparerById<T>.Compare(const Left: T; const Right: T): Integer;
begin
  Result := Left.Id.CompareTo(Right.Id);
end;

class function TEntityComparerById<T>.GetEqualityComparer: IEqualityComparer<T>;
begin
  Result := GetSingleton;
end;

class function TEntityComparerById<T>.GetEqualityComparison:
  TEqualityComparison<T>;
begin
  Result := GetSingleton.Equals;
end;

function TEntityComparerById<T>.Equals(const Left: T; const Right: T): Boolean;
begin
  Result := Left.Id = Right.Id;
end;

class function TEntityComparerById<T>.GetComparer: IComparer<T>;
begin
  Result := GetSingleton;
end;

function TEntityComparerById<T>.GetHashCode(const Value: T): Integer;
begin
  Result := (Value as TObject).GetHashCode;
end;

class function TEntityComparerById<T>.GetSingleton: TEntityComparerById<T>;
begin
  if not Assigned(fSingleton) then
    fSingleton := TEntityComparerById<T>.Create;
  Result := fSingleton;
end;

class destructor TEntityComparerById<T>._destroy;
begin
  FreeAndNil(fSingleton);
end;

{ TTasksHelper }

class function TTasksHelper.TryAsType(const typInfo: PTypeInfo; const value: IInterface;
  out intf): Boolean;
begin
  Result := value.QueryInterface(GetTypeData(typInfo)^.GUID, intf) = S_OK;
end;

class function TTasksHelper.TryAsType<I>(const value: IInterface; out intf: I):
  Boolean;
begin
  Result := TryAsType(TypeInfo(I), value, intf);
end;

{ TJsonObjectHelper }

procedure TJsonObjectHelper.TrimExcess;

  procedure DoTrimExcess(obj: TJsonObject);

    function KeepValue(const dataValue: PJsonDataValue): Boolean;
    var
      j: Integer;
      jsonArray: TJsonArray;
    begin
      Result := True;
      case dataValue^.Typ of
        jdtString:
          begin
            Result := dataValue.Value <> '';
          end;
        jdtObject:
          begin
            if not dataValue.IsNull then
              DoTrimExcess(dataValue.ObjectValue);
          end;
        jdtArray:
          begin
            jsonArray := dataValue^.ArrayValue;
            for j := jsonArray.Count - 1 downto 0 do
            begin
              if not KeepValue(jsonArray.Items[j]) then
                jsonArray.Delete(j);
            end;
          end;
      end;
    end;

  var
    i: Integer;
  begin
    for i := obj.Count - 1 downto 0 do
    begin
      if not KeepValue(obj.Items[i]) then
        obj.Delete(i);
    end;
  end;

begin
  DoTrimExcess(Self);
end;

end.
