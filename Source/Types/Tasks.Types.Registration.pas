unit Tasks.Types.Registration;

interface

uses
  Tasks.Types,
  Spring.Collections;

type
  TTasksTypesRegistrator = class
  private
    class var fResourceTypeInfoMap: IReadOnlyDictionary<Pointer, TResourceDataKind>;
  public
    class constructor Create;
    class procedure BindDefaultResolver; static;
  end;

implementation

uses
  Tasks.Types.Default,
  System.SysUtils;

const
  cDataKindToInterfacedClass: array [TResourceDataKind] of TInterfacedResourceClass = (
    TInterfacedGoogleTask,
    TInterfacedGoogleTaskList,
    TInterfacedGoogleTaskCollection,
    TInterfacedGoogleTaskListCollection
  );

  cDataKindToInterfacedClassSimple: array [TResourceDataKind] of TInterfacedResourceClass = (
    TInterfacedGoogleTask,
    TInterfacedGoogleTaskList,
    TInterfacedGoogleTaskCollectionSimple,
    TInterfacedGoogleTaskListCollectionSimple
  );

class procedure TTasksTypesRegistrator.BindDefaultResolver;
begin
  Tasks.Types.TTasks._resolver :=
    function(typInfo: Pointer): IInterface
    var
      kind: TResourceDataKind; 
    begin
      if not fResourceTypeInfoMap.TryGetValue(typInfo, kind) then
        raise EInvalidOpException.Create('Unknown resource type info')
      else  
        Result := cDataKindToInterfacedClassSimple[kind].Create;
    end;
end;

class constructor TTasksTypesRegistrator.Create;
var
  map: IDictionary<Pointer, TResourceDataKind>;
begin
  map := TCollections.CreateDictionary<Pointer, TResourceDataKind>;
  map.Add(TypeInfo(IGoogleTask), TResourceDataKind.Task);
  map.Add(TypeInfo(IGoogleTaskList), TResourceDataKind.TaskList);
  map.Add(TypeInfo(IGoogleTaskCollection), TResourceDataKind.TaskCollection);
  map.Add(TypeInfo(IGoogleTaskListCollection), TResourceDataKind.TaskListCollection);  
  fResourceTypeInfoMap := map.AsReadOnlyDictionary;
end;

end.
