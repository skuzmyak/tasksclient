unit Tasks.Types.Serializable;

interface

type
  ISelfSerializable = interface
  ['{8E38D8B3-4115-44D0-AF54-71AD058C5491}']
    function GetAsJson: string;
    procedure SetAsJson(const Value: string);

    function ToJson(trimExcess: Boolean = True; compact: Boolean = True): string;
    property AsJson: string read GetAsJson write SetAsJson;
  end;


implementation

end.
