unit Tasks.Types;

interface

uses
  Spring,
  Spring.Collections;

type
  TResourceDataKind = (
    Task,
    TaskList,
    TaskCollection,
    TaskListCollection
  );

  IResource = interface(IInterface)
  ['{216E4944-E569-49AC-AAD2-E2006F38D96D}']
    function GetKind: TResourceDataKind;
    function GetETag: string;

    property Kind: TResourceDataKind read GetKind;
    property ETag: string read GetETag;
  end;

  IResourceEntity = interface(IResource)
  ['{E0447412-1331-49D0-8B07-B2CC3FC35564}']
    function GetId: string;
    function GetTitle: string;
    function GetSelfLink: string;
    function GetLastUpdatedUtc: TDateTime;
    procedure SetTitle(const Value: string);

    property Id: string read GetId;
    property Title: string read GetTitle write SetTitle;
    /// <summary>
    ///   URL pointing to this task. Used to retrieve, update, or delete this task
    /// </summary>
    property SelfLink: string read GetSelfLink;
    /// <summary>
    ///   Last modification time of the task (as a RFC 3339 timestamp).
    /// </summary>
    property LastUpdatedUtc: TDateTime read GetLastUpdatedUtc;
  end;

  TTaskLink = class;

  TGoogleTaskStatus = (
    NeedsAction,
    Completed
  );

  TTaskStatusChangedEvent = procedure(Sender: TObject; status: TGoogleTaskStatus) of object;

  IGoogleTask = interface(IResourceEntity)
  ['{65D65E53-67FC-4CC0-91E9-42DED3576CC2}']
    function GetParentTaskId: string;
    function GetPosition: string;
    function GetStatus: TGoogleTaskStatus;
    function GetDeleted: Boolean;
    function GetHidden: Boolean;
    function GetHasDueDate: Boolean;
    function GetDueUtc: TDateTime;
    function GetCompletedUtc: TDateTime;
    function GetNotes: string;
    function GetLinks: TArray<TTaskLink>;
    function GetOnStatusChange: IEvent<TTaskStatusChangedEvent>;
    procedure SetStatus(const Value: TGoogleTaskStatus);
    procedure SetDueUtc(const Value: TDateTime);
    procedure SetNotes(const Value: string);

    /// <summary>
    ///   Parent task identifier.
    ///This field is omitted if it is a top-level task. This field is read-only.
    ///Use the "move" method to move the task under a different parent or to the top level.
    /// </summary>
    property ParentTaskId: string read GetParentTaskId;
    /// <summary>
    ///   String indicating the position of the task among its sibling tasks under the same parent task
    ///or at the top level. If this string is greater than another task's corresponding position string
    /// according to lexicographical ordering, the task is positioned after the other task under
    ///the same parent task (or at the top level).
    ///This field is read-only. Use the "move" method to move the task to another position.
    /// </summary>
    property Position: string read GetPosition;
    /// <summary>IGoogleTask.Status
    /// Status of the task. This is either "needsAction" or "completed".
    /// </summary>
    /// type:TTaskStatus
    property Status: TGoogleTaskStatus read GetStatus write SetStatus;
    /// <summary>IGoogleTask.Deleted
    /// Flag indicating whether the task has been deleted. The default if False.
    /// </summary>
    /// type:Boolean
    property Deleted: Boolean read GetDeleted;
    /// <summary>IGoogleTask.Hidden
    /// Flag indicating whether the task is hidden. This is the case if the task had
    /// been marked completed when the task list was last cleared. The default is False.
    /// This field is read-only.
    /// </summary>
    /// type:Boolean
    property Hidden: Boolean read GetHidden;
    /// <summary>IGoogleTask.DueUtc
    /// Due date of the task (as a RFC 3339 timestamp). Optional.
    /// </summary>
    /// type:TDateTime
    property DueUtc: TDateTime read GetDueUtc write SetDueUtc;
    property HasDueDate: Boolean read GetHasDueDate;
    /// <summary>IGoogleTask.CompletedUtc
    /// Completion date of the task (as a RFC 3339 timestamp). This field is omitted if
    /// the task has not been completed.
    /// </summary>
    /// type:TDateTime
    property CompletedUtc: TDateTime read GetCompletedUtc;
    /// <summary>
    ///   Notes describing the task. Optional
    /// </summary>
    property Notes: string read GetNotes write SetNotes;
    /// <summary>IGoogleTask.Links
    /// Collection of links. This collection is read-only
    /// </summary> type:TArray<TTaskLink>
    property Links: TArray<TTaskLink> read GetLinks;
    property OnStatusChanged: IEvent<TTaskStatusChangedEvent> read GetOnStatusChange;
  end;

  IGoogleTaskList = interface(IResourceEntity)
  ['{5DB32FE0-9149-4BA5-A803-3B0DBF58F71A}']
  end;

  IResourceCollection<T: IResourceEntity> = interface(IResource)
    function GetItems: IEnumerable<T>;
    property Items: IEnumerable<T> read GetItems;
  end;

  IGoogleTaskCollection = interface(IResourceCollection<IGoogleTask>)
  ['{9E8C7089-8CDC-47C4-9B6F-6EADAC442EB2}']
  end;

  IGoogleTaskListCollection = interface(IResourceCollection<IGoogleTaskList>)
  ['{86F4B730-AEB2-4716-B61B-4E9D4D0EE6FD}']
  end;

  IGoogleTasks = IGoogleTaskCollection;
  IGoogleTaskLists = IGoogleTaskListCollection;

  TTaskLink = class
  strict private
    fType: string;
    fDescription: string;
    fLink: string;
  public
    /// <summary>
    ///  Type of the link, e.g. "email".
    /// </summary>
    property &Type: string read fType write fType;
    /// <summary>
    ///   The description. In HTML speak: Everything between <a> and </a>.
    /// </summary>
    property Description: string read fDescription write fDescription;
    /// <summary>
    ///   The URL
    /// </summary>
    property Link: string read fLink write fLink;
  end;

  /// <summary>
  ///   This record is considered as factory
  /// </summary>
  TTasks = record
  public type
    TResolver = reference to function(typInfo: Pointer): IInterface;
  public
    class var _resolver: TResolver;
  public
    class function Create<I: IResource>: I; static;
    class function CreateTask: IGoogleTask; static;
    class function CreateTaskList: IGoogleTaskList; static;
  end;

implementation

uses
  Tasks.Types.Helpers;

{ TTasks }

class function TTasks.Create<I>: I;
var
  typInfo: PTypeInfo;
begin
  Guard.CheckNotNull(Assigned(_resolver), '_resolver');
  typInfo := TypeInfo(I);
  if not TryAsType(typInfo, _resolver(typInfo), Result) then
    Assert(False);
end;

class function TTasks.CreateTask: IGoogleTask;
begin
  Result := TTasks.Create<IGoogleTask>;
end;

class function TTasks.CreateTaskList: IGoogleTaskList;
begin
  Result := TTasks.Create<IGoogleTaskList>;
end;


end.



