unit Tasks.Client.Service.Tests;

interface

uses
  DUnitX.TestFramework,
  Tasks.Types,
  Tasks.Client;

type

  [TestFixture]
  TTasksServiceTests = class(TObject)
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure Get_CallsBuilder;
    [Test]
    procedure Get_CallsBuilderEx;
    destructor Destroy; override;
  end;

  [TestFixture]
  TTasksServiceFunctionalTests = class(TObject)
  private
    const
      cDefaultListId = '@default';
  private
    fClient: ITasksClient;
    fTestList: IGoogleTaskList;
    fTestTask: IGoogleTask;
  public
    [SetupFixture]
    procedure SetupFixture;
    [TearDownFixture]
    procedure TearDownFixture;
  public
    [Test]
    procedure List_DefaultList_ReturnsValidTaskCollection;
    [Test]
    procedure Get_ExistingTaskId_ReturnsValidTask;
    [Test]
    procedure Insert_NewTaskWithoutId_ReturnsTaskWithId;
    [Test]
    procedure Update_ExistingTask_Success;
    [Test]
    procedure Delete_ExistingTask_Success;
    [Test]
    procedure Clear_ExistingTaskListId_MarksCompletedTasksAsHidden;
  end;

  [TestFixture]
  TTaskListsServiceFunctionalTests = class(TObject)
  private
    fClient: ITasksClient;
    fTestList: IGoogleTaskList;
  public
    [SetupFixture]
    procedure SetupFixture;
    [TearDownFixture]
    procedure TearDownFixture;
  public
    [Test]
    procedure List_ReturnsTaskListCollection;
    [Test]
    procedure Get_DefaultList_ReturnsTaskList;
    [Test]
    procedure Insert_NewList_ReturnsValidList;
    [Test]
    procedure Update_ExistingTask_ReturnsUpdatedList;
    [Test]
    procedure Delete_RemovesTaskFromList;
  end;

implementation

uses
  Tasks.Client.Service.Base,
  Tasks.Client.Service.Tasks,
  Spring.Mocking,
  Tasks.Client.Rest,
  Spring,
  Tasks.Client.RequestBuilder.Default,
  Tasks.Client.RequestBuilder,
  Tasks.Types.Helpers
  , System.DateUtils, System.SysUtils, Spring.Collections.Dictionaries;

type
  TasksClientHelper = record helper for TasksClient
    class function CreateDefault: ITasksClient; static;
  end;

   
  TRequestBuilderMock = class(TRequestBuilder)
  protected
  end;

  TRestRequestMock = class(TRestRequest)
  public
    function Execute: TRestResponse; override;

  end;


  TTasksServiceAccess = class(TTasksService)
  public type
    TPathAccess = TTasksService.TPath;
  end;

  TPathEx = class(TTasksServiceAccess.TPathAccess);

class function TasksClientHelper.CreateDefault: ITasksClient;
begin
  Result := TasksClient.Create(TClientSecrets.ReadFromCredentials(''));
end;


destructor TTasksServiceTests.Destroy;
begin

  inherited;
end;

procedure TTasksServiceTests.Get_CallsBuilder;
var
  service: ITasksService;
  builder: Mock<IRequestBuilder>;
begin
  builder := Mock<IRequestBuilder>.Create(TMockBehavior.Dynamic);
  builder.Setup.Returns<IRequestBuilderTransient>(builder.Instance).When.GET;
  builder.Setup.Returns<IRequestBuilderTransient>(builder.Instance).When.Path(TPathEx.Get);
  builder.Setup.Returns<IRequestBuilderTransient>(builder.Instance).When.AddPathParam(TRequestParam.TaskListId, 'TestTaskListId');
  builder.Setup.Returns<IRequestBuilderTransient>(builder.Instance).When.AddPathParam(TRequestParam.TaskId , 'TestTaskId');
  builder.Setup.Returns<IShared<TRestRequest>>( Shared.Make<TRestRequest>( TRestRequestMock.Create(nil) ) ).When.Build;
  service := TTasksService.Create(builder.Instance);

  // builder.Received()

  try
    service.Get('TestTaskId', 'TestTaskListId');
  except

  end;

  builder.Received(Times.Once).GET;
  builder.Received(Times.Once).Path(TPathEx.Get);
  builder.Received(Times.Once).AddPathParam(TRequestParam.TaskListId, 'TestTaskListId');
  builder.Received(Times.Once).AddPathParam(TRequestParam.TaskId, 'TestTaskId');
  builder.Received(Times.Once).Build;
  Assert.Pass();
end;

procedure TTasksServiceTests.Get_CallsBuilderEx;
var
  service: ITasksService;
begin
  service := TTasksService.Create(TRequestBuilder.Produce(nil));
  try
    service.Get('id', 'id2');
  except

  end;

end;

procedure TTasksServiceTests.Setup;
begin
end;

procedure TTasksServiceTests.TearDown;
begin
end;


{ TRestRequestMock }

function TRestRequestMock.Execute: TRestResponse;
begin
  Result := Self.Response as TRestResponse;
end;

procedure TTasksServiceFunctionalTests.SetupFixture;
var
  lst: IGoogleTaskList;
  task: IGoogleTask;
begin
  fClient := TasksClient.CreateDefault;
  lst := TTasks.Create<IGoogleTaskList>;
  lst.Title := 'test list 001';
  fTestList := fClient.TaskListsService.Insert(lst);

  task := TTasks.CreateTask;
  task.Title := 'test task title';
  fTestTask := fClient.TasksService.Insert(task, fTestList.Id)
end;

procedure TTasksServiceFunctionalTests.TearDownFixture;
var
  list: IGoogleTaskList;
  lists: IGoogleTaskListCollection;
begin
  lists := fclient.TaskListsService.List;

  for list in lists.Items do
  begin
    if list.Title = 'test list 001' then
      fClient.TaskListsService.Delete(list.Id);
  end;
end;

procedure TTasksServiceFunctionalTests.List_DefaultList_ReturnsValidTaskCollection;
var
  service: ITasksService;
  tasks: IGoogleTaskCollection;
begin
  //arrange
  service := fClient.TasksService;

  //act
  tasks := service.List(fTestList.Id);

  //assert
  Assert.IsNotNull(tasks, 'returned tasks is null');
  Assert.IsNotNull(tasks.Items);
  if not tasks.Items.Contains(fTestTask, TTaskComparerById.GetEqualityComparison()) then
    Assert.Fail('tasks collection does not contain test task');
end;

procedure TTasksServiceFunctionalTests.Insert_NewTaskWithoutId_ReturnsTaskWithId;
var
  service: ITasksService;
  task, taskInserted: IGoogleTask;
begin
  //arrange
  service := fClient.TasksService;
  task := TTasks.CreateTask;
  task.Title := 'test task';

  //act
  taskInserted := service.Insert(task, fTestList.Id);

  //assert
  Assert.IsNotNull(taskInserted);
  Assert.IsNotEmpty(taskInserted.id);
  Assert.AreNotSame(task, taskInserted);
  Assert.AreNotEqual(task.Id, taskInserted.Id);
end;

procedure TTasksServiceFunctionalTests.Get_ExistingTaskId_ReturnsValidTask;
var
  service: ITasksService;
  task: IGoogleTask;
begin
  //arrange
  service := fClient.TasksService;

  //act
  task := service.Get(fTestTask.Id, fTestList.Id);

  //assert
  Assert.IsNotNull(task);
  Assert.AreEqual(fTestTask.Id, task.Id);
end;

procedure TTasksServiceFunctionalTests.Update_ExistingTask_Success;
var
  service: ITasksService;
  taskUpdated: IGoogleTask;
begin
  //arrange
  service := fClient.TasksService;

  fTestTask.Title := 'dummy two';
  fTestTask.Status := TGoogleTaskStatus.Completed;
  fTestTask.DueUtc := Trunc(Now + 4);

  //act
  taskUpdated := service.Update(fTestTask, fTestList.Id);

  //assert
  Assert.IsNotNull(taskUpdated);
  Assert.AreEqual(taskUpdated.id, fTestTask.Id);
  Assert.AreEqual<TGoogleTaskStatus>(fTestTask.Status, taskUpdated.Status);
  Assert.AreEqual(fTestTask.Title, taskUpdated.Title);
  Assert.AreEqual(fTestTask.DueUtc, taskUpdated.DueUtc);
  Assert.AreNotSame(fTestTask, taskUpdated);
end;

procedure TTasksServiceFunctionalTests.Delete_ExistingTask_Success;
var
  service: ITasksService;
  task, taskDeleted: IGoogleTask;
begin
  //arrange
  service := fClient.TasksService;
  task := TTasks.CreateTask;
  task.Title := 'To be deleted';
  task := service.Insert(task, fTestList.Id);
  Assert.IsNotEmpty(task.id);

  //act
  service.Delete(task.id, fTestList.Id);

  //assert
  taskDeleted := service.Get(task.Id, fTestList.Id);
  Assert.IsNotNull(taskDeleted);
  Assert.IsTrue(taskDeleted.Deleted);
end;

procedure TTasksServiceFunctionalTests.Clear_ExistingTaskListId_MarksCompletedTasksAsHidden;
var
  service: ITasksService;
  task: IGoogleTask;
begin
  //arrange
  service := fClient.TasksService;
  task := TTasks.CreateTask;
  task.Title := 'To be cleared';
  task.Status := TGoogleTaskStatus.Completed;
  task := service.Insert(task, fTestList.Id);
  Assert.IsNotEmpty(task.id);

  //act
  service.Clear(fTestList.Id);

  //assert
  task := service.Get(task.Id, fTestList.Id);
  Assert.IsNotNull(task);
  Assert.IsTrue(task.Hidden);
end;

procedure TTaskListsServiceFunctionalTests.List_ReturnsTaskListCollection;
var
  service: ITaskListsService;
  lists: IGoogleTaskListCollection;
begin
  service := fClient.TaskListsService;

  lists := service.List;

  Assert.IsNotNull(lists);
  Assert.AreNotEqual(lists.Items.Count, 0);
end;

procedure TTaskListsServiceFunctionalTests.SetupFixture;
var
  lst: IGoogleTaskList;
begin
  fClient := TasksClient.CreateDefault;
  lst := TTasks.Create<IGoogleTaskList>;
  lst.Title := 'test list 001';
  fTestList := fClient.TaskListsService.Insert(lst);
end;

procedure TTaskListsServiceFunctionalTests.TearDownFixture;
var
  list: IGoogleTaskList;
  lists: IGoogleTaskListCollection;
begin
  lists := fclient.TaskListsService.List;

  for list in lists.Items do
  begin
    if list.Title = 'test list 001' then
      fClient.TaskListsService.Delete(list.Id);
  end;
end;

procedure TTaskListsServiceFunctionalTests.Get_DefaultList_ReturnsTaskList;
var
  service: ITaskListsService;
  list: IGoogleTaskList;
begin
  service := fClient.TaskListsService;

  list := service.Get('@default');

  Assert.IsNotNull(list);
  Assert.IsNotEmpty(list.Id);
end;

procedure TTaskListsServiceFunctionalTests.Insert_NewList_ReturnsValidList;
const
  cTitle = 'super dummy TITLE ~$%';
var
  service: ITaskListsService;
  list, listInserted: IGoogleTaskList;
begin
  service := fClient.TaskListsService;
  list := TTasks.CreateTaskList;
  list.Title := cTitle;

  listInserted := service.Insert(list);

  service.Delete(listInserted.Id);//cleanup
  Assert.IsNotNull(listInserted);
  Assert.AreNotEqual(list, listInserted);
  Assert.IsNotEmpty(listInserted.Id);
  Assert.AreEqual(list.Title, listInserted.Title);
end;

procedure TTaskListsServiceFunctionalTests.Update_ExistingTask_ReturnsUpdatedList;
var
  service: ITaskListsService;
  list, listUpdated: IGoogleTaskList;
begin
  service := fClient.TaskListsService;
  list := fTestList;
  list.Title := list.Title + 'updated title';

  listUpdated := service.Update(list);

  Assert.IsNotNull(listUpdated);
  Assert.AreNotEqual(list, listUpdated);
  Assert.IsNotEmpty(listUpdated.Id);
  Assert.AreEqual(list.Title, listUpdated.Title);
end;

procedure TTaskListsServiceFunctionalTests.Delete_RemovesTaskFromList;
var
  service: ITaskListsService;
  list: IGoogleTaskList;
  lists: IGoogleTaskListCollection;
begin
  service := fClient.TaskListsService;
  list := fTestList;

  service.Delete(list.Id);

  lists := service.List;
  if lists.Items.Contains(list, TTaskListComparerById.GetEqualityComparison()) then
    Assert.Fail('contains deleted list');
  Assert.Pass();
end;


initialization
  TDUnitX.RegisterTestFixture(TTasksServiceFunctionalTests);
  TDUnitX.RegisterTestFixture(TTaskListsServiceFunctionalTests);
  TDUnitX.RegisterTestFixture(TTasksServiceTests);
end.
