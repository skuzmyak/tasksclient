unit Tasks.Client.Tests;

interface

uses
  DUnitX.TestFramework;

type
  [TestFixture]
  TTasksClientDefaultTests = class
  public
    [Test]
    procedure GetTaskListsService_Ok;
  end;


implementation

uses
  Tasks.Startup,
  Tasks.Client,
  Tasks.Client.Default,
  Tasks.Client.Registration,
  Tasks.Types;

{ TTasksClientDefaultTests }

procedure TTasksClientDefaultTests.GetTaskListsService_Ok;
var
  client: ITasksClient;
//  lst: IGoogleTaskListCollection;
begin
  client := TasksClient.Create(TClientSecrets.ReadFromCredentials(''));
//  settings := TTasksClientSettings.Create;
//  settings.LoadFromCredentials(ParamStr(0));
//   client := TTasksClient.Create(
//    TTasksClientRegistrator.TasksServiceFactory(),
//    TTasksClientRegistrator.TaskListsServiceFactory(),
//    settings
//  );

//  lst := client.TaskListsService.List;

end;

initialization
  TDUnitX.RegisterTestFixture(TTasksClientDefaultTests);

end.
