unit Tasks.Types.Classes.Tests;

interface
uses
  DUnitX.TestFramework;

type

  [TestFixture]
  TResourceTest = class(TObject)
  public
    procedure ResourceClass_HasVirtualConstructor;
  end;

implementation

uses
  Tasks.Types;
  
//type
//  TResourceMock = class(TResource);

procedure TResourceTest.ResourceClass_HasVirtualConstructor;
begin
  
end;

initialization
  TDUnitX.RegisterTestFixture(TResourceTest);
end.
