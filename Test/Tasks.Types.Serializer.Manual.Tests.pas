unit Tasks.Types.Serializer.Manual.Tests;

interface

uses
  DUnitX.TestFramework,
  Tasks.Types.Serializer.Manual,
  Spring;

type

  [TestFixture]
  TResourceSerializerManualTests = class(TObject)
  private
    fSerializer: TResourceSerializerManual;
  private
    procedure AssertTaskResource(value: TObject); 
     
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    [Test]
    procedure DoSerialize_TaskResource_ReturnsValidJson;
    [Test]
    procedure DoSerialize_TasksResource_ReturnsValidJson;
  end;

implementation

uses
  Tasks.Types, 
  Tasks.Types.Classes, 
  Tasks.Types.Serializer.Base,
  System.SysUtils, JsonDataObjects, System.Variants, Tasks.Types.Helpers;


procedure TResourceSerializerManualTests.DoSerialize_TasksResource_ReturnsValidJson;
const
  cTaskCount = 10;
var
  task: TGoogleTask;
  tasks: IShared<TGoogleTaskCollection>;
  i: Integer;
  jsonObj: IShared<TJsonObject>;
  jsonStr: string;
begin
  //arrange
  tasks := Shared.Make<TGoogleTaskCollection>(TGoogleTaskCollection.Create);
  for i := 0 to cTaskCount - 1 do
  begin
    task := TGoogleTask.Create();
    task.DueUtc := IncMonth(Now, i);
    task.Title := 'test title_' + IntToStr(i);
    task.Notes := 'test notes_' + IntToStr(i);
    if i mod 2 = 0 then
      task.Status := TGoogleTaskStatus.Completed
    else
      task.Status := TGoogleTaskStatus.NeedsAction;

    tasks.Items.Add(task);
  end;

  //act
  jsonStr := fSerializer.DoSerialize(tasks);

  //assert
  jsonObj := Shared<TJsonObject>.Make;
  jsonObj.FromJSON(jsonStr);

  Assert.AreEqual(cTaskCount, jsonObj.A[TResource.TProperty.Items].Count, 'tasks array is not the same');

  for i := 0 to cTaskCount - 1 do
  begin
    AssertTaskResource(tasks.Items[i])
  end;  
end;

procedure TResourceSerializerManualTests.Setup;
begin
  fSerializer := TResourceSerializerManual.Create;
end;

procedure TResourceSerializerManualTests.TearDown;
begin
  fSerializer.Free;
end;

procedure TResourceSerializerManualTests.DoSerialize_TaskResource_ReturnsValidJson;
var
  task: IShared<TGoogleTask>;
begin
  //setup
  task := Shared.Make<TGoogleTask>(TGoogleTask.Create());
  task.DueUtc := Now;
  task.Title := 'test title';
  task.Notes := 'test notes';
  task.Status := TGoogleTaskStatus.Completed;

  AssertTaskResource(task);
end;

procedure TResourceSerializerManualTests.AssertTaskResource(value: TObject);
var
  jsonObj: IShared<TJsonObject>;
  jsonStr: string;
  task: TGoogleTask absolute value;
begin
  //act
  jsonStr := fSerializer.DoSerialize(task);

  //assert
  jsonObj := Shared<TJsonObject>.Make;
  jsonObj.FromJSON(jsonStr);

  Assert.AreEqual(task.DueUtc, jsonObj.DUtc[TResource.TProperty.Due], 'task due is not the same');
  Assert.AreEqual(task.Title, jsonObj.S[TResource.TProperty.Title], 'task title is not the same');
  Assert.AreEqual(task.Notes, jsonObj.S[TResource.TProperty.Notes], 'task notes is not the same');
  Assert.AreEqual(task.Status.AsString, jsonObj.S[TResource.TProperty.Status], 'task status is not the same')
end;

initialization
  TDUnitX.RegisterTestFixture(TResourceSerializerManualTests);
end.
