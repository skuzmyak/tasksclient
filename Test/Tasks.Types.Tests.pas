unit Tasks.Types.Tests;

interface
uses
  DUnitX.TestFramework;

type

  [TestFixture]
  TResourceFactoryTests = class(TObject)
  public
    [Test]
    procedure NewResource_ReturnsAssignedInstance;
    [Test, Ignore]
    procedure NewResource_SupportsResourceRef;
    [Test]
    procedure NewResource_UnknownResourceType_ThrowException;
    [Test]
    procedure ResourceTypes_HasUniqueGuid;
    [Test]
    procedure NewGoogleTask_SupportsAncestorInterfaces;
    [Test]
    procedure NewGoogleTaskList_SupportsAncestorInterfaces;
    [Test]
    procedure NewGoogleTaskCollection_SupportsAncestorInterfaces;
    [Test]
    procedure NewGoogleTaskListCollection_SupportsAncestorInterfaces;
    [Test]
    procedure NewResourcec_InstanceHasValidGuid;
    [Test]
    procedure NewResource_RefCounted;
  end;

implementation

uses
  Tasks.Types,
  Tasks.Types.Classes,
  Tasks.Types.Registration,
  
  System.TypInfo,
  System.SysUtils,
  System.Types,

  Spring;

procedure TResourceFactoryTests.NewResource_ReturnsAssignedInstance;
var
  value: IResource;
begin
  value := TTasks.Create<IGoogleTask>;
  Assert.IsNotNull(value);

  value := TTasks.Create<IGoogleTaskList>;
  Assert.IsNotNull(value);

  value := TTasks.Create<IGoogleTaskCollection>;
  Assert.IsNotNull(value);

  value := TTasks.Create<IGoogleTaskListCollection>;
  Assert.IsNotNull(value);
end;

procedure TResourceFactoryTests.NewResource_SupportsResourceRef;
var
  ref: IResourceReference;
begin
  ref := Assert.Implements<IResourceReference>(TTasks.Create<IGoogleTask>);
  Assert.IsNotNull(ref());

  ref := Assert.Implements<IResourceReference>(TTasks.Create<IGoogleTaskList>);
  Assert.IsNotNull(ref());

  ref := Assert.Implements<IResourceReference>(TTasks.Create<IGoogleTaskCollection>);
  Assert.IsNotNull(ref());

  ref := Assert.Implements<IResourceReference>(TTasks.Create<IGoogleTaskListCollection>);
  Assert.IsNotNull(ref());
end;

type
  IDummyTask = interface(IResourceEntity)
  ['{C225F335-97D1-4054-BF59-55C574072672}']
  end;

procedure TResourceFactoryTests.NewResource_UnknownResourceType_ThrowException;
var
  test: TProc;
begin
  test :=
    procedure
    begin
      TTasks.Create<IDummyTask>;
    end;

  Assert.WillRaiseAny(test);
end;

procedure TResourceFactoryTests.ResourceTypes_HasUniqueGuid;
begin
  Assert.AreNotEqual(IGoogleTask, GUID_NULL, 'task type msut have unique guid');

  Assert.AreNotEqual(IGoogleTaskList, GUID_NULL, 'task list type must have unique guid');

  Assert.AreNotEqual(IGoogleTaskCollection, GUID_NULL, 'tasks type guid must be unique');

  Assert.AreNotEqual(IGoogleTaskListCollection, GUID_NULL, 'task lists type guid must be unique');
end;

procedure TResourceFactoryTests.NewResourcec_InstanceHasValidGuid;
var
  obj: TObject;
  res: HRESULT;
begin

  res := TTasks.Create<IGoogleTask>.QueryInterface(IGoogleTask, obj);
  Assert.AreEqual(res, S_OK, 'returned task has bad guid');

  res := TTasks.Create<IGoogleTaskList>.QueryInterface(IGoogleTaskList, obj);
  Assert.AreEqual(res, S_OK, 'returned tasklist has bad guid');

  res := TTasks.Create<IGoogleTaskCollection>.QueryInterface(IGoogleTaskCollection, obj);
  Assert.AreEqual(res, S_OK, 'returned tasks has bad guid');

  res := TTasks.Create<IGoogleTaskListCollection>.QueryInterface(IGoogleTaskListCollection, obj);
  Assert.AreEqual(res, S_OK, 'returned task lists has bad guid');
end;

procedure TResourceFactoryTests.NewGoogleTask_SupportsAncestorInterfaces;
const
  cFormat = '%s does not support %s';
var
  name: string;
begin
  name := GetTypeName(TypeInfo(IGoogleTask));
  Assert.Implements<IResource>(TTasks.Create<IGoogleTask>, Format(cFormat, [name, GetTypeName(TypeInfo(IResource))]));
  Assert.Implements<IResourceEntity>(TTasks.Create<IGoogleTask>, Format(cFormat, [name, GetTypeName(TypeInfo(IResourceEntity))]));
end;

procedure TResourceFactoryTests.NewGoogleTaskList_SupportsAncestorInterfaces;
const
  cFormat = '%s does not support %s';
var
  name: string;
begin
  name := GetTypeName(TypeInfo(IGoogleTaskList));
  Assert.Implements<IResource>(TTasks.Create<IGoogleTaskList>, Format(cFormat, [name, GetTypeName(TypeInfo(IResource))]));
  Assert.Implements<IResourceEntity>(TTasks.Create<IGoogleTaskList>, Format(cFormat, [name, GetTypeName(TypeInfo(IResourceEntity))]));
end;

procedure TResourceFactoryTests.NewGoogleTaskCollection_SupportsAncestorInterfaces;
const
  cFormat = '%s does not support %s';
var
  name: string;
begin
  name := GetTypeName(TypeInfo(IGoogleTaskCollection));
  Assert.Implements<IResource>(TTasks.Create<IGoogleTaskCollection>, Format(cFormat, [name, GetTypeName(TypeInfo(IResource))]));
end;

procedure TResourceFactoryTests.NewGoogleTaskListCollection_SupportsAncestorInterfaces;
const
  cFormat = '%s does not support %s';
var
  name: string;
begin
  name := GetTypeName(TypeInfo(IGoogleTaskListCollection));
  Assert.Implements<IResource>(TTasks.Create<IGoogleTaskListCollection>, Format(cFormat, [name, GetTypeName(TypeInfo(IResource))]));
end;

procedure TResourceFactoryTests.NewResource_RefCounted;
var
  intf: IInterface;
  ref: Weak<IInterface>;
begin
  intf := TTasks.Create<IGoogleTask>;
  ref := intf;
  intf := nil;
  Assert.IsNull(ref, 'factory must return ref counted instance of task');

  intf := TTasks.Create<IGoogleTaskList>;
  ref := intf;
  intf := nil;
  Assert.IsNull(ref, 'factory must return ref counted instance of task list');

  intf := TTasks.Create<IGoogleTaskCollection>;
  ref := intf;
  intf := nil;
  Assert.IsNull(ref, 'factory must return ref counted instance of tasks');

  intf := TTasks.Create<IGoogleTaskListCollection>;
  ref := intf;
  intf := nil;
  Assert.IsNull(ref, 'factory must return ref counted instance of task lists');
end;

initialization
  TTasksTypesRegistrator.BindDefaultResolver;
  TDUnitX.RegisterTestFixture(TResourceFactoryTests);
end.
